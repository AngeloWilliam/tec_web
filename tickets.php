<?php

  require_once "bootstrap.php";
  require_once "init_stylesheet_script.php";
  require_once "utils/init_navbar_footer.php";

  if(!isset($_GET["evento"])) {
    header('Location: http://eventsfinder.altervista.org/index.php');
  } else {
    //Base Template
    $templateParams["title"] = "Events Finder - Evento";
    $templateParams["pageRequested"] = "ticket.php";

    $templateParams["stylesheet"] = get_stylesheets_as_array();
    //array_push($templateParams["stylesheet"], 'rel = "stylesheet" href = "./css/event.css"');
    $templateParams["js"] = get_script_as_array();
    array_push($templateParams["js"], 'src="./javascript/ticket.js"');

    $evento_richiesto = get_event($_GET["evento"], $dbh);
    $biglietti_evento = $dbh -> getBigliettiEvento($_GET["evento"]);

    $templateParams = array_merge($templateParams, $evento_richiesto);
    $templateParams["Tickets"] = array();
    foreach($biglietti_evento as $biglietto){
        array_push($templateParams["Tickets"], $biglietto);
    }
    //$templateParams = array_merge($templateParams, $biglietti_evento);

    //Home Template
//  $templateParams["articoli"] = $dbh -> getPosts(2);

  }
  require 'template/base.php';
?>