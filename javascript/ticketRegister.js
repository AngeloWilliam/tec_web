﻿$(document).ready(function () {
    $("#btnRegister").click(function () {
        let prezzo = $("#prezzoeuro").val() + '.' + $("#prezzocent").val();
        let biglietti = parseInt($("#totbiglietti").val());
        let nome = $("#nome").val();
        let tmp = ["tipobiglietto", nome, prezzo, biglietti];
        const tab_body = $("#tab_body");

        $.getJSON("js_server_communicator.php", { "richiesta": tmp }, function (result) {
            console.log(result);
            if (result > 0) {
                tab_body.append(
                    `<tr class="biglietto">
                            <td>${nome}</td>
                            <td class="text-center">${prezzo}</td>
                            <td class="text-center">${biglietti}</td>
                         </tr>`);

            }
        });
    });

    $("#btnDone").click(function () {
        console.log("immahere");
        if ($(".biglietto").length > 0) {
            //Se esiste almeno un biglietto posso uscire/reindirizzare alla home
            if(confirm("Creazione effettuata con successo!"))
                window.location.replace(" http://eventsfinder.altervista.org/index.php");
            else
                window.location.replace(" http://eventsfinder.altervista.org/index.php");
            
        }
        else {
            //Comunica di registrare almeno un biglietto
            alert("Devi registrare almeno un biglietto!");
        }
    });
});