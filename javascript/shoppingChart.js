function rimuoviBiglietti(idItem) {
    if (confirm('Sicuro di voler rimuovere i biglietti?')) {
        // Save it!
        console.log('Biglietti in rimozione.');
        jQuery.ajax({
            type: "POST",
            url: './php/tickets_js_functions.php',
            dataType: 'json',
            data: {functionname: 'delete_tickets', arguments: idItem},

            success: function (obj, textstatus) {
                if (!('error' in obj)) {
                    console.log("Inside if");
                    console.log(obj);
                    // var v = JSON.parse(obj);
                    // console.log(v);
                    console.log(textstatus);
                    alert("Biglietti rimossi dal carrello!");
                    isCarrelloVuoto();

                    document.getElementById("biglietto" + idItem).remove();
                    $(".totale_valore").text(obj.totale + " €");
                } else {
                    console.log(obj.error);
                    console.log("qualcosa è andato storto");
                    alert("Rimozione non avvenuta, riprovare a rimuovere biglietti.");
                }
            }
        });


    } else {
        // Do nothing!
        console.log('Non rimuovere i biglietti selezionato.');
    }
}

function isCarrelloVuoto() {
    jQuery.ajax({
        type: "POST",
        url: './php/tickets_js_functions.php',
        dataType: 'json',
        data: {functionname: 'are_there_tickets'},

        success: function (obj, textstatus) {
            if (!('error' in obj)) {
                if (!obj.areThereTickets) {
                    console.log("Non ci sono più biglietti nel carrello");
                    document.location.reload();
                }
            } else {
                console.log(obj.error);
            }
        }
    });
}

function acquistoBiglietti() {
    var stringaDomandaAcquisto = "Procedere all'acquisto?\n\n" +
        "NB: Ricordiamo che la presenza dei biglietti nel carrello non consiste in una prenotazione.\n" +
        "Verificheremo quindi, che durante la permanenza nel carrello non siano stati " +
        "acquistati da nessun altro. ";


    if (confirm(stringaDomandaAcquisto)) {
        jQuery.ajax({
            type: "POST",
            url: './php/tickets_js_functions.php',
            dataType: 'json',
            data: {functionname: 'buy_tickets'},

            success: function (obj, textstatus) {
                console.log(obj.debug);
                if (!('error' in obj)) {

                    ///rimuovere punto esclamativo
                    if (obj.bigliettiDisponibili) {
                        alert("Acquisto andato a buon fine!");
                        location.replace('http://eventsfinder.altervista.org/newStructure/')

                    } else {
                        var alertMsg = "Ci dispiace!";
                        for (i = 0; i < obj.itemPassed.length; i++) {

                            if (obj.itemPassed[i].venduto) {
                                console.log("miao");
                            } else {
                                var biglietto = obj.itemPassed[i].biglietto;
                                var evento = obj.itemPassed[i].evento;
                                console.log("Nome evento: " + evento.NomeEvento);
                                console.log("Nome biglietto: " + biglietto.Nome);
                                alertMsg += "\n\nPer l'evento \"" + evento.NomeEvento + "\" sono rimasti " +
                                    (biglietto.TotBiglietti - biglietto.NumVenduti) + " biglietti di tipo " +
                                    biglietto.Nome + ".";
                            }
                        }
                        alert(alertMsg);

                    }
                    console.log(obj);
                } else {
                    console.log(obj.error);
                }
            }
        });
    }
}
