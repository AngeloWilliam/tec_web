﻿$(document).ready(function () {
    makeAdminCall("Utenti", 0);
    /*
    $.getJSON("js_server_communicator.php", { "richiesta": tmp }, function (result) {
        let scelta = $("#selectManage option:selected").val();
        if (scelta== "Utenti")
            result.push("Utenti");
        else
            result.push("Eventi");

        getItems(result);
    });*/

    $("#selectManage").change(function () {
        let scelta = $("#selectManage option:selected").val();
        makeAdminCall(scelta, 0);
        /*
        $.post({
            url: 'js_server_communicator.php', "richiesta": tmp, success: function (result) {
                console.log(result);
                let scelta = $("#selectManage option:selected").val();
                if (scelta == "Utenti")
                    result.push("Utenti");
                else
                    result.push("Eventi");

                getItems(result);
            }
        });*/
    });

    $("#btnForward").click(function () {
        let id = $("#btnForward").data("Id");
        let scelta = $("#selectManage option:selected").val();
        makeAdminCall(scelta, id);
    });

    $("#btnBackward").click(function () {
        let id = $("#btnBackward").data("Id");
        let scelta = $("#selectManage option:selected").val();
        makeAdminCall(scelta, id);
    });
});

function makeAdminCall(scelta, id) {
    console.log(id);
    let tmp;
    if (scelta == "Utenti")
        tmp = ["lista_utenti", id];
    else
        tmp = ["lista_eventi", id];

    $.getJSON("js_server_communicator.php", { "richiesta": tmp }, function (result) {
        console.log(result);

        getItems(result, "Amministratori");
    });
}

