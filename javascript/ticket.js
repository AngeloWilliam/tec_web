$(document).ready(function () {
});

/*
* In pratica prendevo gli elementi dall'url per poi ndare a fare richiesta al database dei biglietti rimasti.
* Più in basso calcolo del totale in base a biglietti selezionati.*
*/
function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}

function wantPutTickets() {
    if(confirm("Aggiungere biglietti al carrello?")){
        putTickets();
    }
}
function putTickets() {

    console.log("ciao Amici");

    var ticketsData = document.getElementById('ticketsCost').value.split("|");
    var ticketsCost = ticketsData[0];
    var ticketsID = ticketsData[1];

    var ticketsQuantity = document.getElementById('ticketsQuantity').value;

    var ins = "inserimentoOk";
    var tca = "bigliettiCarrello"
    var tns = "bigliettiNonVenduti"

    console.log("Costo = " + ticketsCost);
    console.log("Quantità = " + ticketsQuantity);

    $("#totale").text("Totale: " + (ticketsCost * ticketsQuantity).toFixed(2) + " €");

    console.log('getUrlParameter(\'evento\') = ' + getUrlParameter('evento'));

    /*
     * Stavo preparando la richiesta da fare al database. In base all "echo" fatto nel file getnumber...
     * ottengo il numero dei biglietti disponibili rimasti.
     *
     * Devo recuperare l'id del biglietto richiesto.
     */

    var IDEvento = getUrlParameter("evento");
    var xmlhttp = new XMLHttpRequest();
    console.log("dopo var xml assegnamento");
    xmlhttp.onreadystatechange = function () {
        console.log("Inside if callback");
        if (this.readyState === 4 && this.status === 200) {

            var v = JSON.parse(this.response);
            console.log(this.response);
            console.log(v);
            console.log(v[ins]);
            console.log(v[tca]);
            console.log(v[tns]);

            if (v[ins]) {
                console.log("Dentro else quantità = " + ticketsQuantity);
                console.log("Biglietti ancora disponibili " + parseInt(this.responseText));
                alert("Biglietti inseriti nel carrello!");
                console.log("Inside if2");
            } else if (v[tns] <= 0) {
                // console.log("Dentro if quantità = " + ticketsQuantity);
                // console.log("Biglietti ancora disponibili " + this.responseText);
                console.log("Biglietti Terminati");
                alert("Ci dispiace, ma questi biglietti sono terminati.");

                console.log("Inside if else");
            } else if (v[tns] < ticketsQuantity) {
                console.log("Biglietti richiesti > disponibili");
                alert("Il numero di biglietti selezionati è maggiore di quelli disponibili\n" +
                    "I biglietti disponibili sono: " + v[tns]);
            } else {
                console.log("biglietti carrello + disponibili < zero");
                alert("Impossibile aggiungere.\n" +
                    " Nel carrello hai già " + v[tca] + " biglietti.\n"
                    + "I biglietti ancora disponibili sono " + v[tns]);
            }
        }
    };
    xmlhttp.open("GET", "./php/putTicketsChart.php?IDEvento=" + IDEvento + "&IDBiglietto=" + ticketsID + "&NumeroBiglietti=" + ticketsQuantity, true);
    // xmlhttp.open("GET", "./javascript/prova.php", true);
    console.log("xmlhttp aperta");
    xmlhttp.send();
    console.log("xmlhttp inviata");
}

