﻿function fillList(data) {
    let lista = $('#listEvent');
    /*data.forEach(function (item) {
        lista.append(`<option class="opzEvento" >` + item["NomeEvento"] + `</option>`);
        $("#listEvent option:last-child").data("idevento", item["Id"]);
    });
    /*$(".opzEvento").each(function () {
        console.log($(this).data("idevento"));
    });*/
    for (let i = 0; i < data.length; i++) {
        lista.append(`<option class="opzEvento" >` + data[i]["NomeEvento"] + `</option>`);
        $("#listEvent option:last-child").data("idevento", data[i]["Id"]);
    };
}

$(document).ready(function () {
    let lista = $('#listEvent');
    let tmp = ["eventi_organizzatore"];
    console.log("Ho caricato minimo lo script");
    if (lista.length > 0) {
        $.getJSON("js_server_communicator.php", { "richiesta": tmp }, function (data) {
            console.log("Sono entrato");
            console.log(data);
            fillList(data);
        });
    }

    $("#btnSend").click(function () {
        let lista = $('#listEvent');
        let evento;
        if (lista.length > 0)
            evento = $('#listEvent option:selected').data("idevento");
        else
            evento = 0;

        let tmp = ["messaggio", $('#oggetto').val(), $('#messaggio').val(), evento];
        console.log(tmp);
        /*
        $.post({
            url: 'js_server_communicator.php', "richiesta": tmp , success: function (result) {
                console.log("Messaggio inviato");
                console.log(result);
                //TODO: Informa l'utente che il messaggio è stato inviato
            }
        });*/
        $.getJSON("js_server_communicator.php", { "richiesta": tmp }, function (data) {
            console.log("Messaggio inviato");
            console.log(data);
            fillList(data);
        });
    });
});