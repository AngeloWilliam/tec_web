﻿
function showMessages(messaggi, usertype) {
    let result = `<div class="accordion" id="messageList">`;
    let tmp;

    for (let i = 0; i < messaggi.length; i++) {
        if (usertype == "Organizzatori")
            tmp = "";
        else
            tmp = `${messaggi[i]["NomeEvento"]} -`;

        let messaggio = `
            <div class="card">
		        <div class="card-header" id="heading${messaggi[i]["Id"]}" >
			        <h2 class="mb-0">
				        <button class="btn btn-link collapsed btnElem" type="button" data-toggle="collapse" data-target="#collapse${messaggi[i]["Id"]}" aria-expanded="false" aria-controls="collapse${messaggi[i]["Id"]}">
					        ${messaggi[i]["DataInvio"]} - ${tmp} ${messaggi[i]["Oggetto"]}
				        </button>
			        </h2>
		        </div>
		        <div id="collapse${messaggi[i]["Id"]}" class="collapse" aria-labelledby="heading${messaggi[i]["Id"]}" data-parent="#messageList">
			        <div class="card-body">
				        <p>
					        ${messaggi[i]["Testo"]}
				        </p>
			        </div>
		        </div>
	        </div>
        `;
        result += messaggio;
    }
    result += `</div>`;
    

    return result;
}

function showUsers(utenti) {
    let result = `<ul class="list-group" id="userList">`;

    for (let i = 0; i < utenti.length; i++) {
        let utente = `
                        <li class="list-group-item" id="userItem${utenti[i]["Id"]}">
                            <div class="row">
                                <div class="col-11">
                                    Utente:<b>${utenti[i]["Username"]}</b>      Tipo:<b>${utenti[i]["TipoUtente"]}</b>      Mail:<b>${utenti[i]["Mail"]}</b>
                                </div>
                                <div class="col">
                                    <button class="btn btn-primary btnElem" type="button" id="userBtn${utenti[i]["Id"]}">Elimina</button>
                                </div>
                            </div>
						</li>
        `;
        result += utente;
    }
    result += `</ul>`;

    return result;
}

function showEvents(eventi) {
    let result = ``;
    for (let i = 0; i < eventi.length; i++) {
        console.log(eventi[i]);
        if (i === 0 || i % 2 === 0) {
            result += `<div class="row">`;
        }
        result += `<div class="col-md-6 py-1" id="${eventi[i]["Id"]}">
                  <div class="row px-0 align-items-center text-left text-dark">

                    <!-- Colonna per immagine -->
                    
                        <div class="col-sm-7">
                          <img src="${eventi[i]["Immagine"]}" alt="" class="event_image">
                        </div>
                    

                    <!-- Colonna per dati evento -->
                    <div class="col-sm-5 pl-sm-0 eventoRapp">
                      <p><a href="event.php?evento=${eventi[i]["Id"]}" class="text-decoration-none" > ${eventi[i]["NomeEvento"]} </a></p>
                      <p> ${eventi[i]["DataInizio"]} </p>
                      <p class="font-italic"> Luogo: ${eventi[i]["Luogo"]}</p>
                      <p><button class="btn btn-primary btnElem" type="button" id="eventoBtn${eventi[i]["Id"]}">Elimina</button></p>  
                    </div>
                  </div>
              </div>`;

        if (i % 2 === 1 || (i + 1) === eventi.length) {
            result += `</div>`;
        }

    }

    return result;
}

//Metodo che richiama la giusta funzione per riempire la lista
//e aggiunge funzionalità agli elementi della lista (tipo il settaggio
//di una notifica come letta se gli si clicca sopra).
function getItems(items, $usertype) {
    let itemType = items[items.length - 1];
    items.pop();
    if (items[items.length - 1] == true) {
        $("#btnForward").show();
    } else {
        $("#btnForward").hide();
    }
    items.pop();

    const listContainer = $("#listContainer");
    listContainer.empty();

    if (items.length < 1) {
        listContainer.append("Qui è tutto vuoto...");
        return;
    }

    let itemList;
    switch (itemType) {
        case "Messaggi":
            if (typeof items[0]["NomeEvento"] === "undefined")
                itemList = showMessages(items, "Organizzatori");
            else
                itemList = showMessages(items, "");
            break;
        case "Utenti":
            itemList = showUsers(items);
            break;
        case "Eventi":
            itemList = showEvents(items);
            break;
        default:
            break;
    }

    listContainer.append(itemList);
    $("#btnBackward").data("Id", items[0]["Id"] + 11);
    $("#btnForward").data("Id", items[items.length - 1]["Id"]);
    

    $(".btnElem").each(function (index) {
        $(this).data("Id", items[index]["Id"]);
        switch (itemType) {
            case "Messaggi":
                if (items[index]["NotificationStatus"] > 0) {
                    $(this).addClass("text-secondary");
                }
                break;
            default:
                break;
        }
    });

    $(".btnElem").click(function () {
        let thisItem = $(this).data("Id");
        let actionType;
        switch (itemType) {
            case "Messaggi":
                actionType = "notifica_letta";
                $(this).addClass("text-secondary");
                break;
            case "Utenti":
                actionType = "elimina_utente";
                $(this).addClass("text-secondary");
                break;
            case "Eventi":
                actionType = "elimina_evento";
                $(this).addClass("text-secondary");
                break;
            default:
                break;
        }
        var toSend = [actionType, thisItem];
        /*
        $.post({
            url: 'js_server_communicator.php', "richiesta": toSend, success: function (result) {
                console.log(result);
            }
        })*/
        $.getJSON("js_server_communicator.php", { "richiesta": toSend }, function (data) {
            console.log(data);
        });
    });
}

