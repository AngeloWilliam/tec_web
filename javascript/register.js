$(document).ready(function () {
    console.log("bottone cliccato");
});

function registrazione(tipologiaUtente){
    var username = $("#usname").val();
    var name = $("#name").val();
    var surname = $("#surname").val();
    var mail = $("#email").val();
    var piva = $("#piva").val();
    var psw = $("#psw").val();
    var confPsw = $("#confirm_psw").val();
    var esitoRegistrazione = false;

    console.log("usname " + username);
    console.log("name " + name);
    console.log("surname " + surname);
    console.log("mail " + mail);
    console.log("piva " + piva);
    console.log("psw " + psw);
    console.log("confpsw " + confPsw);

    if (psw !== confPsw) {
        alert("Password di conferma non corrisponde");
        return false;
    }

    var arguments = {
        'username' : username,
        'name' : name,
        'surname' : surname,
        'mail' : mail,
        'piva' : piva,
        'psw' : psw,
        'confPsw' : confPsw,
        'tipologiaUtente' : tipologiaUtente
    };
    if (confirm("Confermi?")) {
        jQuery.ajax({
            type: "POST",
            url: './php/tickets_js_functions.php',
            dataType: 'json',
            // async: false,
            data: {functionname: 'register_utente', arguments: arguments},

            success: function (obj, textstatus) {
                console.log(obj.debug);
                if (!('error' in obj)) {
                    if (obj.ripetizione){
                        alert("Questo indirizzo mail è gia presente nel database");
                    } else {
                        esitoRegistrazione = true;
                        alert("Registrazione andata a buon fine");
                        document.location.reload();
                    }
                    console.log(obj);
                } else {
                    alert("La registrazione non è andata a buon fine");
                }
            }
        });
    }

    console.log("miao");
    console.log("esito registrazione " + esitoRegistrazione);
    return esitoRegistrazione;
}



