﻿$(document).ready(function () {
    let tmp = ["ricercaPresel"];
    let a = $("#loggedIn").text();
    if (a.length > 1) {
        if (a.indexOf("name") >= 0) {
            tmp.push("name");
            tmp.push(a.slice(5, a.length));
        }
        else if (a.indexOf("place") >= 0) {
            tmp.push("place");
            tmp.push(a.slice(6, a.length));
        }

    }
    console.log(tmp);

    $.getJSON("js_server_communicator.php", { "richiesta": tmp }, function (data) {
        console.log(data);
        fillList(data);
    });


    $("#btnCerca").click(function () {
        let nome, luogo, date;

        nome = $('#nomeEvento').val();
        luogo = $('#luogoEvento').val();
        date = $('#dataEvento').val();

        let tmp = ["ricercaEventi", nome, luogo, date];

        $.getJSON("js_server_communicator.php", { "richiesta": tmp }, function (data) {
            console.log(data);
            fillList(data);
        });
    });
});

function fillList(data) {

    /*  Il javascript mi serve per fare modifiche alla pagina senza dover
     *  ricaricarla, ovvero senza fare richieste http. E' utile anche per associare 
     *  agli elementi delle informazioni che potrebbero servirmi.
     *  
     *  In questo specifico caso, utilizzo il javascript per associare
     *  ad ogni evento nella lista degli eventi il suo id, così che quando invio il messaggio
     *  prendo l'id da quell'elemento.
     *  
     *  Come invio il messaggio? Vai dentro simpleServRequest.php
     */
    const lista = $('#eventList');
    lista.empty();
    data.forEach(function (item) {
        /*if (item["Descrizione"].length > 100)
            tmp = item["Descrizione"].substring(0, 97) + "...";
        else
            tmp = item["Descrizione"];
        
         * Valido per il list group;
        lista.append(`  <li>
				            <h3>` + item["NomeEvento"] + ` - ` + item["Luogo"] + `</h3>
				            <h5>` + item["DataInizio"] + ` - ` + item["DataFine"] + `</h5>
				            <p>` + tmp + `</p>
			            </li>`);*/

        lista.append(`
                <div class="col-md-6 py-1">
                    <a href="event.php?evento=` + item["Id"] + `" class="text-decoration-none">
                        <div class="row px-0 align-items-center text-left text-dark">
                            <!-- Colonna per immagine -->
                            <div class="col-sm-7">
                                <img src="`+item["Immagine"]+`" alt="" class="event_image">
                            </div>

                            <!-- Colonna per dati evento -->
                            <div class="col-sm-5 pl-sm-0">
                                <p>`+item["NomeEvento"]+`</p>
                                <p>`+ item["DataInizio"] + ` / ` + item["DataFine"] + `</p>
                                <p class="font-italic"> `+item["Luogo"]+`</p>
                            </div>
                        </div>
                    </a>
                </div>
            `);
                        
    });
}


