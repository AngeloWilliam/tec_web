﻿$(document).ready(function () {
});

/*
* In pratica prendevo gli elementi dall'url per poi ndare a fare richiesta al database dei biglietti rimasti.
* Più in basso calcolo del totale in base a biglietti selezionati.*
*/
function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}

function prova() {
    console.log("ciao Amici");

    var ticketsData = document.getElementById('ticketsCost').value.split("|");
    var ticketsCost = ticketsData[0];
    var ticketsID = ticketsData[1];

    var ticketsQuantity = document.getElementById('ticketsQuantity').value;

    var ins = "inserimentoOk";
    var tca = "bigliettiCarrello"
    var tns = "bigliettiNonVenduti"

    console.log("Costo = " + ticketsCost);
    console.log("Quantità = " + ticketsQuantity);

    console.log('getUrlParameter(\'evento\') = ' + getUrlParameter('evento'));

    /*
     * Stavo preparando la richiesta da fare al database. In base all "echo" fatto nel file getnumber...
     * ottengo il numero dei biglietti disponibili rimasti.
     *
     * Devo recuperare l'id del biglietto richiesto.
     */

    var IDEvento = getUrlParameter("evento");

    let tmp = [IDEvento, ticketsID, ticketsQuantity];

    $.getJSON("./php/putTicketsChartAlberto.php", { "acquistoBiglietti": tmp }, function (data) {

        if (data[ins]) {
            console.log("Dentro else quantità = " + ticketsQuantity);
            console.log("Biglietti ancora disponibili " + parseInt(this.responseText));
            alert("Biglietti inseriti nel carrello!");
            console.log("Inside if2");
        } else if (data[tns] <= 0) {
            // console.log("Dentro if quantità = " + ticketsQuantity);
            // console.log("Biglietti ancora disponibili " + this.responseText);
            console.log("Biglietti Terminati");
            alert("Ci dispiace, ma questi biglietti sono terminati.");

            console.log("Inside if else");
        } else if (data[tns] < ticketsQuantity) {
            console.log("Biglietti richiesti > disponibili");
            alert("Il numero di biglietti selezionati è maggiore di quelli disponibili\n" +
                "I biglietti disponibili sono: " + data[tns]);
        } else {
            console.log("biglietti carrello + disponibili < zero");
            alert("Impossibile aggiungere.\n" +
                " Nel carrello hai già " + data[tca] + " biglietti.\n"
                + "I biglietti ancora disponibili sono " + data[tns]);
        }
    });
}

