﻿$(document).ready(function () {
    if ($(".alter_event").length > 0 && $("#alter_multiple").length > 0) {
        $('input:radio[name=period]')[1].checked = true;
    }
    else {
        $('input:radio[name=period]')[0].checked = true;
    }

    $('input[type=radio][name=period]').change(function () {
        const divHide = $("#divHide");
        if (this.value == "one") {
            divHide.css("display", "none");
        } else if (this.value == "multiple") {
            divHide.css("display", "block");
        }

    });

    if ($(".alter_event").length > 0) {
        let tmp = ["img_alter"];
        const griglia = $("#imageCont");
        let result = "";
        $.getJSON("js_server_communicator.php", { "richiesta": tmp }, function (data) {
            result = fillGrid(data);
            griglia.append(result);
        });
    }
    
    $("#btnSubmit").click(function () {
        let tmp = ["registra_evento"];
        $.post({
            url: "js_server_communicator.php", "richiesta": tmp, success: function (result) {
                console.log(result);
            }
        });
        $.ajax({
            url: 'js_server_communicator.php',
            type: 'POST',
            "richiesta": tmp,
            contentType: "application/json",
            success: function (result) {
                console.log(result);
            }
        });
    });
});

function fillGrid(data) {

    let result = "";
    for (i = 0; i < data.length; i++) {
        result += `<div class="col-6 col-md-3 casella_evento">
            <div class="row event_image_row">
                <div class="col">
                    <img class="event_image" src="./images/${data[i]["Immagine"]}"  alt="">
				</div>
            </div>
        </div>`;
    }

    return result;
}