﻿<?php

  require_once "bootstrap.php";
  require_once "init_stylesheet_script.php";
  require_once "utils/init_navbar_footer.php";

  //Base Template
  $templateParams["title"] = "Events Finder - Supporto/Faq";

  $templateParams["pageRequested"] = "supporto-form.php";

  $templateParams["stylesheet"] = get_stylesheets_as_array();
  array_push($templateParams["stylesheet"], 'rel = "stylesheet" href = "./css/newHomepageCSS.css"');
  $templateParams["js"] = get_script_as_array();
  array_push($templateParams["js"], 'src = "./javascript/screenResizer.js"');

  require 'template/base.php';
?>