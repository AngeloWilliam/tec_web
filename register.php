﻿<?php

  require_once "bootstrap.php";
  require_once "init_stylesheet_script.php";
  require_once "utils/init_navbar_footer.php";
  require_once "utils/functions.php";
  //Base Template
  if(isUserLoggedIn()) {
    header('location: ./');
  }
  $templateParams["title"] = "Events Finder - Registrazione";
  $templateParams["pageRequested"] = "register-form.php";
  $templateParams["stylesheet"] = get_stylesheets_as_array();
  array_push($templateParams["stylesheet"], 'rel = "stylesheet" href = "./css/login.css"');
  $templateParams["js"] = get_script_as_array();
  array_push($templateParams["js"], 'src = "./javascript/register.js"');
  if($_GET['tipo'] == 1) {
    $templateParams['tipoRegistrazione'] = 'Organizzatori';
  } else {
    $templateParams['tipoRegistrazione'] = 'Clienti';
  }
  $templateParams["pageId"] = "login";
  require 'template/base.php';
?>