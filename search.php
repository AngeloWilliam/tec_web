<?php

  require_once "bootstrap.php";
  require_once "init_stylesheet_script.php";
  require_once "utils/init_navbar_footer.php";

  if(!isset($_GET["place"]) && !isset($_GET["name"])) {
    header('Location: http://eventsfinder.altervista.org/index.php');
  } else {
    //Base Template
    $templateParams["title"] = "Events Finder - Cerca";
    $templateParams["pageRequested"] = "event-searchedAlberto.php";
	$templateParams["pageId"] = "searchedEvents";
    $templateParams["stylesheet"] = get_stylesheets_as_array();
    array_push($templateParams["stylesheet"],
               'rel = "stylesheet" href = "./css/search.css"');
    $templateParams["js"] = get_script_as_array();
	array_push($templateParams["js"], 'src = "./javascript/searchEvent.js"');

  }
  require 'template/base.php';
?>