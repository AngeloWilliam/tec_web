﻿<?php
	require_once "bootstrap.php";
	require_once "init_stylesheet_script.php";
	require_once "utils/init_navbar_footer.php";

	$_SESSION["boardType"]="Utenti";

	//Base Template
	$templateParams["title"] = "Events Finder - Amministrazione";
	$templateParams["pageRequested"] = "board-form.php";

	$templateParams["stylesheet"] = get_stylesheets_as_array();
	$templateParams["js"] = get_script_as_array();

    array_push($templateParams["js"], 'src="./javascript/buildBoard.js"');
	array_push($templateParams["js"], 'src="./javascript/adminBoard.js"');

	require 'template/base.php';
?>