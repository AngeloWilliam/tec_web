<?php

  require_once "bootstrap.php";
  require_once "init_stylesheet_script.php";
  require_once "utils/init_navbar_footer.php";
  if(!isset($_GET["evento"])) {
    header('Location: http://eventsfinder.altervista.org');
  } else {
    //Base Template
    $templateParams['usable'] = "disabled";
    if(isUserLoggedIn()) {
      $templateParams['usable'] = "";
    }
    $templateParams["title"] = "Events Finder - Evento";
    $templateParams["pageRequested"] = "event-page.php";
    $templateParams["pageId"] = "event-page";
    $templateParams["stylesheet"] = get_stylesheets_as_array();
    array_push($templateParams["stylesheet"], 'rel = "stylesheet" href = "./css/event.css"');
    $templateParams["js"] = get_script_as_array();
    $evento_richiesto = get_event($_GET["evento"], $dbh);
    $templateParams = array_merge($templateParams, $evento_richiesto);
    /*
     * AGGIUNTO PER BIGLIETTI
     */
    array_push($templateParams["js"], 'src="./javascript/ticket.js"');
    $biglietti_evento = $dbh -> getBigliettiEvento($_GET["evento"]);
    $templateParams["Tickets"] = array();
    foreach($biglietti_evento as $biglietto) {
      array_push($templateParams["Tickets"], $biglietto);
    }

  }
  require 'template/base.php';
?>