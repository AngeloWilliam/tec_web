<?php


  class InfoPrinting {

    /**
     * Print the content of array passed like a preformatted text.
     * @param $array_about String info about kindness of data inside array.
     * @param $array ArrayObject that will be printed. It could be eaven an arrays' arrays
     */
    public static function print_preformatted_array_info($array_about, $array) {

      echo "<pre>";
      echo "Array about : $array_about\n\n";
      echo print_r($array);
      echo "</pre>";
    }

  }


  /**
   * Take an array of events' arrays an add to every "event's array" a key "Immagine" with
   * value the name of image file.
   * @param $events array - Array of event's arrays.
   * @return array  An array where every event's array has a new key "Immagine"
   */
  function get_array_event_with_image($events) {

    $dbh = new DatabaseHelper("localhost", "eventsfinder", "", "my_eventsfinder");
    $array_events_with_image = array();
    foreach($events as $event) {
      if(isset($event['Id'])) {
        $image_event = $dbh -> getEventoImages($event["Id"])[0]["Immagine"];
      } else {
        $image_event = $dbh -> getEventoImages($event["IdEvento"])[0]["Immagine"];
      }
      if(isset($image_event)) {
        $event["Immagine"] = IMG_DIR . $image_event;
      }
      array_push($array_events_with_image, $event);
    }

    return $array_events_with_image;
  }


  function get_event($event, $dbh) {

    $res = $dbh -> getSingoloEvento($event);
    $res[0]["Immagine"] = array();
    $immagini = $dbh -> getEventoImages($event);
    foreach($immagini as $immagine) {
      array_push($res[0]["Immagine"], $immagine["Immagine"]);
    }

    return $res[0];
  }


?>
