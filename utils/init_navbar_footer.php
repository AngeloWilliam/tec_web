<?php

  $templateParams["navbar"] = "componentspage/navbar/navbarClienteLoggato.php";
  if(isUserLoggedIn()) {
	$notifiche = $dbh->checkNotifications($_SESSION["Id"]);
	if(count($notifiche) > 0){
		$templateParams["navbarLoggedOrNotPart"] =
		'<a href="./messageBoard.php">Notifiche(' . count($notifiche) .')</a>';
	}
	else{
		$templateParams["navbarLoggedOrNotPart"] =
		'<a href="./messageBoard.php">Notifiche</a>' ;
	}
	switch($_SESSION["tipoUtente"]) {
      case "Clienti":
        $templateParams["navbarLoggedOrNotPart"] = $templateParams["navbarLoggedOrNotPart"] . '<a href="./shopping_chart.php">Carrello</a>';
        break;
      case "Organizzatori":
		$templateParams["navbarLoggedOrNotPart"] = $templateParams["navbarLoggedOrNotPart"] . '<a href="./organizzatoreBoard.php">Eventi Organizzati</a>';
        break;
	  case "Amministratori":
		$templateParams["navbarLoggedOrNotPart"] = $templateParams["navbarLoggedOrNotPart"] . '<a href="./adminBoard_Users.php">Amministrazione</a>';
		break;
    }


    $templateParams["navbarLoggedOrNotPart"] =
      '<a href="./profile.php">Profilo</a>'
      . $templateParams["navbarLoggedOrNotPart"]
      . '<a href="./logout.php">Log out</a>' ;


  } else {
    $templateParams["navbarLoggedOrNotPart"] = '<a href="./login.php">Login</a>' .
     '<a href="./register.php">Registrati</a>';
  }
  $templateParams["footer"] = "componentspage/footer/footer.html";
?>
