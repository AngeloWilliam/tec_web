<?php

function isUserLoggedIn(){
    return !empty($_SESSION['Id']);
}

function registerLoggedUser($utente){
	$_SESSION["Id"] = $utente["Id"];
	$_SESSION["NomeUtente"] = $utente["Username"];
	$_SESSION["Mail"] = $utente["Mail"];
	$_SESSION["tipoUtente"] = $utente["TipoUtente"];
}

function logOut(){
	unset($_SESSION['Id']);
	unset($_SESSION['NomeUtente']);
	unset($_SESSION['Mail']);
	unset($_SESSION['tipoUtente']);
}

function checkRegistration($tipoutente, $nomeutente, $mail, $password, $nome, $cognome, $piva){
	$result = array(false,"");
	
	if($tipoutente == "Clienti" || $tipoutente=="Organizzatori"){
		if($tipoutente == "Clienti" && isset($nomeutente) && isset($mail) && isset($password)){
			$result[0] = true;
		}
		else{
			$result[1] = "Riempire tutti i campi!";
		}
		if($tipoutente="Organizzatori" && isset($nomeutente) && isset($mail) && isset($password) && isset($nome) && isset($cognome) && isset($piva)){
			$result[0] = true;
		}
		else{
			$result[1] = "Riempire tutti i campi!";
		}
	}
	else{
		$result[1] = "Tipo utente non identificato!";
	}

	return $result; 
}

function rearrangeUpload($files){
	$keys = array_keys($files);
	foreach($keys as $elem){
		for($i = 0; $i < count($files[$elem]); $i++){
			$result[$i][$elem] = $files[$elem][$i];
		}
	}
	return $result;
}

function uploadImage($path, $image){
    $imageName = basename($image["name"]);
    $fullPath = $path.$imageName;
    
    $maxKB = 1000;
    $acceptedExtensions = array("jpg", "jpeg", "png", "gif");
    $result = 0;
    $msg = "";

    //Controllo se immagine è veramente un'immagine
    $imageSize = getimagesize($image["tmp_name"]);
    if($imageSize === false) {
        $msg .= "File caricato non è un'immagine! ";
    }
    
	//Controllo dimensione dell'immagine < 1000KB
    if ($image["size"] > $maxKB * 1024) {
        $msg .= "File caricato pesa troppo! Dimensione massima è $maxKB KB. ";
    }

    //Controllo estensione del file
    $imageFileType = strtolower(pathinfo($fullPath,PATHINFO_EXTENSION));
    if(!in_array($imageFileType, $acceptedExtensions)){
        $msg .= "Accettate solo le seguenti estensioni: ".implode(",", $acceptedExtensions);
    }

    //Controllo se esiste file con stesso nome ed eventualmente lo rinomino
    if (file_exists($fullPath)) {
        $i = 1;
        do{
            $i++;
            $imageName = pathinfo(basename($image["name"]), PATHINFO_FILENAME)."_$i.".$imageFileType;
        }
        while(file_exists($path.$imageName));
        $fullPath = $path.$imageName;
    }

    //Se non ci sono errori, sposto il file dalla posizione temporanea alla cartella di destinazione
    if(strlen($msg)==0){
        if(!move_uploaded_file($image["tmp_name"], $fullPath)){
            $msg.= "Errore nel caricamento dell'immagine.";
        }
        else{
            $result = 1;
            $msg = $imageName;
        }
    }
    return array($result, $msg);
}


?>