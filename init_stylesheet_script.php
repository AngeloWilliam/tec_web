<?php
  function get_stylesheets_as_array(){
    $a = 'rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"';
    $b = 'rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"';
    $c = 'rel="stylesheet" href="./componentspage/navbar/navbarClienteLoggato.css"';
    $d = 'rel = "stylesheet" href = "./css/footer.css"';
    return array($a, $b, $c, $d);
  }

  function get_script_as_array(){
//    $a = 'src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
//      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
//      crossorigin="anonymous"';
    $a = 'src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"';
    $b = 'src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"';
    $c = 'src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
      crossorigin="anonymous"';
    $d = 'src="./componentspage/navbar/navbarClienteLoggato.js"';
    $e = 'src="./javascript/homepage.js"';
    return array($a, $b, $c, $d, $e);
  }
  ?>
