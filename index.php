<?php

  require_once "bootstrap.php";
  require_once "init_stylesheet_script.php";
  require_once "utils/init_navbar_footer.php";

  //Base Template
  $templateParams["title"] = "Events Finder";



  $templateParams["pageRequested"] = "home.php";
  $templateParams["pageId"] = "home";

  $templateParams["stylesheet"] = get_stylesheets_as_array();
  array_push($templateParams["stylesheet"], 'rel = "stylesheet" href = "./css/newHomepageCSS.css"');
  $templateParams["js"] = get_script_as_array();

  /*
   * In realtà questi sono nuovi per ora
   */
  $templateParams["importantEvents"] = get_array_event_with_image($dbh -> getEventi("Nuovi", 4));

  /* ********** DA MODIFICARE ******************** */
  $templateParams["hotEvents"] = get_array_event_with_image($dbh -> getEventi("Nuovi", 4));
  //Home Template
//  $templateParams["articoli"] = $dbh -> getPosts(2);
  require 'template/base.php';
?>