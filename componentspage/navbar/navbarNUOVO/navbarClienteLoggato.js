var login = true;

/**
 * Funzione che inserisce i link nella navbar a seconda che un utente sia loggato o meno.
 *
 * @param isLogged Se true, la navbar viene impostata come se si avesse fatto il login.
 */
function logged(isLogged){
    $('div.panel').empty();
    let m;
    if (isLogged){
        m = `<a href="#about" class="login">Notifiche</a>
        <a href="#about" class="login">Carrello</a>
        <a href="#about" class="login">Profilo</a>`;
    } else {
        m = `<a href="#about" class="login">Login</a>`;
    }

    $('div.panel').append(m);
}

/**
 *
 * @param s
 */
function myFunction(s) {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
        if (s === 'search'){
            $('.panel').attr('class', 'panel no_display');
            $('form').attr('class', '');
        } else {
            $('.panel').attr('class', 'panel');
            $('form').attr('class', 'no_display');
        }
    } else {
        x.className = "topnav";
        $('.panel').attr('class', 'panel');
        $('form').attr('class', '');
    }
}