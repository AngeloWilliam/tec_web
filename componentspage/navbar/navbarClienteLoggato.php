<header>
<div class="topnav" id="myTopnav">
  <a href="./" id="logo_navbar">
    <img id="image_logo_navbar" src="./../componentspage/navbar/logo/logo_small.png"
         alt="website logo">
  </a>
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">

        <form action="search.php" method="get" class=""
              id="search_form_id">
          <label for="searchInput">Nome evento</label>
          <input class="search_text form-control" type="search"
                 placeholder="Inserisci il nome dell'evento che stai cercando"
                 id="searchInput" title="name" name="name">
        </form>
      </div>
    </div>
  </div>
  <a href="javascript:void(0);" id="search_button" onclick="myFunction('search')">
    <div id="magnify">
      &#9906;
    </div>
  </a>
  <a href="javascript:void(0);" id="burger_button" onclick="myFunction('burg')">
    <span class="fa fa-bars"></span>
  </a>

  <div class="panel">
    <?php
      echo $templateParams["navbarLoggedOrNotPart"];
    ?>
  </div>
</div>
</header>