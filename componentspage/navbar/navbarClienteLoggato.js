/**
 *
 * @param s
 */
function myFunction(s) {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
        if (s === 'search'){
            $('.panel').attr('class', 'panel no_display');
            $('#myTopnav > div.container-fluid').attr('class', 'container-fluid');
        } else {
            $('.panel').attr('class', 'panel');
            $('#myTopnav > div.container-fluid').attr('class', 'container-fluid no_display');
        }
    } else {
        x.className = "topnav";
        $('.panel').attr('class', 'panel');
        $('#myTopnav > form').attr('class', '');
    }
}