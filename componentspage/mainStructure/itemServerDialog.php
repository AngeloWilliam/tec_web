﻿<?php
	require_once "./../../bootstrap.php";

	if(isset($_REQUEST["requestType"]) && isset($_REQUEST["id"])){
		switch($_REQUEST["requestType"])){
			case:"deleteMessage":
				$dbh->setNotificationread(,$_REQUEST["id"]);
				break;
			case:"deleteUser":
				break;
			case:"messageWasRead":
				break;
			default:
				break;
		}
	}

	switch($templateParams["pageBox"]){
		case "Messaggi":
			if(isset($_REQUEST["forId"]))
				$items = $dbh->getMessages(1,$_REQUEST["forId"]);
			else
				$items = $dbh->getMessages(1,0);
			break;
		case "Utenti":
			if(isset($_REQUEST["forId"]))
				$items = $dbh->getListaUtenti("Utenti",$_REQUEST["forId"]);
			else
				$items = $dbh->getListaUtenti("Utenti",0);
			break;
		default:
			break;
	}
	
	if(count($items) > 10){
		array_pop($items);
		$anotherPage = true;
	}
	else
		$anotherPage = false;

	array_push($items, $anotherPage);
	array_push($items,$templateParams["pageBox"]);

	header('Content-Type: application/json');
	echo json_encode($items);
?>