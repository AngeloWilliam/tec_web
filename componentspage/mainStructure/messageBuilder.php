﻿<?php
	require_once "./../../bootstrap.php";
	
	if(isset($_REQUEST["id"])){
		switch($templateParams["pageBox"]){
			case "Utenti":
				$dbh->deleteUser($_REQUEST["id"]);
				break;
			case "Messaggi":
				$dbh->setNotificationRead($_SESSION["Id"],$_REQUEST["id"]);
				break;
			default:
				break;
		}
	}
	
	switch($templateParams["pageBox"]){
		case "Messaggi":
			if(isset($_REQUEST["forId"]))
				$items = $dbh->getMessages(1,$_REQUEST["forId"]);
			else
				$items = $dbh->getMessages(1,0);
			break;
		case "Utenti":
			if(isset($_REQUEST["forId"]))
				$items = $dbh->getListaUtenti("Utenti",$_REQUEST["forId"]);
			else
				$items = $dbh->getListaUtenti("Utenti",0);
			break;
		default:
			break;
	}
	
	if(count($items) > 10){
		array_pop($items);
		$anotherPage = true;
	}
	else
		$anotherPage = false;

	array_push($items, $anotherPage);
	array_push($items,$templateParams["pageBox"]);

	header('Content-Type: application/json');
	echo json_encode($items);
?>