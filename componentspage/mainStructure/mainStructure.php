﻿<!DOCTYPE html>
<?php
	require_once "./../../bootstrap.php";
?>
<html lang="it">
	<head>
		<meta charset="utf-8">
		<meta content="width=device-width, initial-scale=1.0" name="viewport">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="stylesheet"
			  href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
			  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
			  crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
		<link rel="stylesheet"
			  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="./css/mainStructure.css">
		<link rel="stylesheet" href="./../footer/css/footer.css">
		<link rel="stylesheet" href="./../navbar/navbarNUOVO/navbarClienteLoggato.css">
		<script src="./../navbar/navbarNUOVO/navbarClienteLoggato.js"></script>
		<script src="./javascript/mainStructure.js"></script>
		<script src="./javascript/jquery-3.4.1.min.js"></script>
		<?php 
			/*	Pagina contenitore, ha sempre un header e un footer e cambia solo il contenuto interno
				Ogni qualvolta vorrò cambiare dovrò prima cambiare la variabile che contiene il nome della
				pagina in cui voglio andare e poi dovrò ricaricare mainStructure.

				In questa sezione in particolare faccio anche il cambio degli script, caricando quello che serve di volta
				in volta.

				Controlla sendMessage.php per continuare con i commenti/tutorial.
			*/

			$templateParams["pagetype"]="eventCreation";
			switch($templateParams["pagetype"]){
				case "listabiglietti":
					echo '<script src="./../../profile/javascript/ticketRegisterCode.js"></script>';
					break;
				case "messagebox":
					echo '<script src="./javascript/messageBox_Alberto.js"></script>';
					break;
				case "adminbox":
					echo '<script src="./javascript/messageBox_Alberto.js"></script>';
					break;
				case "resizeEvent":
					echo '<script src="./javascript/resizeEvent.js"></script>';
					break;
				case "sendMessage":
					echo '<script src="./../../profile/javascript/sendMessage.js"></script>';
					break;
				case "searchEvent":
					echo '<script src="./../../profile/javascript/searchEvent.js"></script>';
					break;
			}
		?>		
		<title><?php echo $templateParams["title"];?></title>
	</head>
	<body>
		<section id="nav">
			<?php include("./../navbar/navbarNUOVO/navbarClienteLoggato.html") ?>
		</section>
		<section id="main_content">
			<div class="container-fluid" id="immagine_dietro">
					<!-- Includere qui la pagina necessaria -->
					<?php 
						switch($templateParams["pagetype"]){
							case "listabiglietti":
								include("./../../profile/ticketRegister.php");
								break;
							case "adminbox":
								include("./../../profile/adminBox_Alberto.php");
								break;
							case "searchEvent":
								include("./../../profile/searchAlberto.php");
								break;
							case "profilo":
								include("./../../profile/profileAlberto.php");
								break;
							case "messagebox":
								include("./../../profile/messageBox_Alberto.php");
								break;
							case "resizeEvent":
								include("./../../profile/eventAlberto.php");
								break;
							case "sendMessage":
								include("./../../profile/sendMessage.php");
								break;
							case "alter_event":
								$_SESSION["alter_event"]=true;
								include("./../../profile/event_creationAlberto.php");
								break;
							case "register":
								include("./../../login/register.php");
								break;
							case "eventCreation":
								$_SESSION["alter_event"]=false;
								include("./../../profile/event_creationAlberto.php");
								break;
						}
						/*$templateParams["tipoPagina"] = "homepage";
						include("simpleComponentsAlberto.php");*/
					?>
			</div>
		</section>
		<section id="other_content">
		</section>
		<?php include("./../footer/footer.html") ?>
	</body>
</html>
