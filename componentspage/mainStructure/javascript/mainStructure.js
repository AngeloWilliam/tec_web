window.onbeforeunload = function () {
    window.scrollTo(0, 0);
}

function hideDate() {
    var radioOne = document.getElementById("radioOne");
    var radioMultiple = document.getElementById("radioMultiple");
    var x = document.getElementById("divHide");
    if (radioOne.checked == true)
        x.style.display = "none";
    if (radioMultiple.checked == true)
        x.style.display = "block";
}
