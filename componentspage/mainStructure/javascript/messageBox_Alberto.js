﻿function showMessages(messaggi) {
    let result = `<div class="accordion" id="messageList">`;

    for (let i = 0; i < messaggi.length; i++) {
        let messaggio = `
            <div class="card">
		        <div class="card-header" id="heading${messaggi[i]["Id"]}" >
			        <h2 class="mb-0">
				        <button class="btn btn-link collapsed btnElem" type="button" data-toggle="collapse" data-target="#collapse${messaggi[i]["Id"]}" aria-expanded="false" aria-controls="collapse${messaggi[i]["Id"]}">
					        ${messaggi[i]["DataInvio"]} - ${messaggi[i]["NomeEvento"]} - ${messaggi[i]["Oggetto"]}
				        </button>
			        </h2>
		        </div>
		        <div id="collapse${messaggi[i]["Id"]}" class="collapse" aria-labelledby="heading${messaggi[i]["Id"]}" data-parent="#messageList">
			        <div class="card-body">
				        <p>
					        ${messaggi[i]["Testo"]}
				        </p>
			        </div>
		        </div>
	        </div>
        `;
        result += messaggio;
    }
    result += `</div>`;

    return result;
}

function showUsers(utenti) {
    let result = `<ul class="list-group" id="userList">`;

    for (let i = 0; i < utenti.length; i++) {
        let utente = `
                        <li class="list-group-item" id="userItem${utenti[i]["Id"]}">
                            <div class="row">
                                <div class="col-11">
                                    Utente:<b>${utenti[i]["Username"]}</b>      Tipo:<b>${utenti[i]["TipoUtente"]}</b>      Mail:<b>${utenti[i]["Mail"]}</b>
                                </div>
                                <div class="col">
                                    <button class="btn btn-primary btnElem" type="button" id="userBtn${utenti[i]["Id"]}">Elimina</button>
                                </div>
                            </div>
						</li>
        `;
        result += utente;
    }
    result += `</ul>`;

    return result;
}

function showEvents(eventi) {
    let result = ``;
    for (let i = 0; i < eventi.length; i++) {
        if (i === 0 || i % 2 === 0) {
            result += `<div class="row">`;
        }
        result += `<div class="col-md-6 py-1">
                <a href="event.php?evento=${eventi[i]["Id"]}" class="text-decoration-none">
                  <div class="row px-0 align-items-center text-left text-dark">

                    <!-- Colonna per immagine -->
                    <div class="col-sm-7">
                      <img src="${eventi[i]["Immagine"]}" alt="" class="event_image">
                    </div>

                    <!-- Colonna per dati evento -->
                    <div class="col-sm-5 pl-sm-0">
                      <p> ${eventi[i]["NomeEvento"]} </p>
                      <p> ${eventi[i]["DataInizio"]} </p>
                      <p class="font-italic"> Luogo: ${eventi[i]["Luogo"]}</p>
                    </div>
                  </div>
                </a>
              </div>`;

        if(i%2 === 1 || (i+1) === eventi.length){
            result += `</div>`;
        }

    }

    return result;
}


function getItems(items) {
    console.log(items);
    let itemType = items[items.length - 1];
    items.pop();
    if (items[items.length - 1] == true) {
        $("#btnForward").show();
    } else {
        $("#btnForward").hide();
    }
    $("#btnBackward").data("id", items[0]["Id"] + 11);
    $("#btnForward").data("id", items[items.length - 2]["Id"]);
    items.pop();
    let itemList;
    switch (itemType) {
        case "Messaggi":
            itemList = showMessages(items);
            break;
        case "Utenti":
            itemList = showUsers(items);
            break;
        case "Eventi":
            itemList = showEvents(items);
            break;
        default:
            break;
    }
    const listContainer = $("#listContainer");
    listContainer.empty();    
    listContainer.append(itemList);
    $(".btnElem").each(function (index) {
        $(this).data("id", items[index]["Id"]);
        switch (itemType) {
            case "Messaggi":
                if (items[index]["NotificationStatus"] > 0) {
                    $(this).addClass("text-secondary");
                }
                break;
            default:
                break;
        }
    });

    $(".btnElem").click(function () {
        let lastItem = $("#btnBackward").data("id");
        let thisItem = $(this).data("id");
        var toSend = { forId: lastItem, id: thisItem };
        $.post({
            url: 'messageBuilder.php', data: toSend, success: function (result) {
                console.log(result);
            }
        })
        switch (itemType) {
            case "Messaggi":
                $(this).addClass("text-secondary");
                break;
            case "Utenti":
                $(this).addClass("text-secondary");
                break;
            case "Eventi":
                break;
            default:
                break;
        }
    });
}

$(document).ready(function () {
    $.getJSON("messageBuilder.php", function (data) {
        console.log(data);
        getItems(data);
    });

    $("#btnForward").click(function () {
        var forId = {forId: $("#btnForward").data("id")};
        $.post({
            url: 'messageBuilder.php', data: forId, success: function (result) {
                getItems(result);
            }
        })
    });

    $("#btnBackward").click(function () {
        var forId = {forId: $("#btnBackward").data("id")};
        $.post({
            url: 'messageBuilder.php', data: forId, success: function (result) {
                getItems(result);
            }
        })
    });
});