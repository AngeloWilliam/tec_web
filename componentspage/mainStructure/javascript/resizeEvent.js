﻿$(document).ready(function () {
    if ($(window).width() < 1280)
        $(".imgElem").addClass("col-10");
    else if ($(window).width() < 1600)
        $(".imgElem").addClass("col-8");
    else
        $(".imgElem").addClass("col-4");
});