<?php

  require_once "bootstrap.php";
  require_once "init_stylesheet_script.php";
  require_once "utils/init_navbar_footer.php";
  //CAMBIARE LOGGGGGIIIIIN
  if(!isUserLoggedIn()) {
    header('Location: http://eventsfinder.altervista.org');
  } else {
    $templateParams["title"] = "Events Finder - Carrello";
    $templateParams["stylesheet"] = get_stylesheets_as_array();
    array_push($templateParams["stylesheet"], 'rel = "stylesheet" href = "./css/profile.css"');
    $templateParams["js"] = get_script_as_array();
    array_push($templateParams["js"],'src="./javascript/shoppingChart.js"');
    /*InfoPrinting::print_preformatted_array_info("templateParams", $templateParams);*/
    if(!isset($_SESSION["Chart"]) || count($_SESSION["Chart"]) <= 0) {
      $templateParams["pageRequested"] = "empty_chart.php";
    } else {
      $templateParams["pageRequested"] = "chart.php";
	  $templateParams["pageId"] = "searchedEvents";
      $templateParams["listaCarrello"] = array();
      $templateParams["totale"] = 0;
      foreach($_SESSION["Chart"] as $id_biglietto => $acquisto) {
        $evento = $dbh -> getSingoloEvento($acquisto["IDEvento"]);
        $evento = get_array_event_with_image($evento)[0];
        $tipo_biglietto = $dbh -> getSingoloBiglietto($id_biglietto)[0];
        $tipo_biglietto["NelCarrello"] = $acquisto["NumeroBiglietti"];
        $item_carrello = array(
          "evento" => $evento,
          "biglietto" => $tipo_biglietto
        );
        $templateParams["totale"] += $acquisto["Totale"];
        array_push($templateParams["listaCarrello"], $item_carrello);
      }
    }
  }
  require 'template/base.php';
