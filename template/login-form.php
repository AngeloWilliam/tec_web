

          <h1 id="zone_title">Login</h1>

          <form id="input_data_account" action="#" method="POST">

            <label for="email">
              <b>Indirizzo mail</b>
            </label>
            <input type="email" placeholder="Inserire indirizzo mail" name="email"
                   id="email"
                   required="">

            <label for="psw">
              <b>Password</b>
            </label>
            <input type="password" placeholder="Enter Password" name="psw" id="psw"
                   required="">

            <?php if(isset($templateParams["errorelogin"])): ?>
              <div class="incorrect"><?php echo $templateParams["errorelogin"]; ?></div>
            <?php endif; ?>

            <p class="psw"><a href="./register.php">Forgot password?</a></p>

            <button type="submit">Login</button>

          </form>
