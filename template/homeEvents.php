﻿<div class="eventi">
  <div class="container-fluid">
    <div class="row">
      <div class="col type_events text-center">
        <h2>Eventi più importanti della settimana</h2>
      </div>
    </div>
    <div class="row events_row justify-content-around">
      <?php
        if(isset($templateParams["importantEvents"])):
          foreach($templateParams["importantEvents"] as $importantEvent):
            ?>
            <div class="col-6 col-md-3 casella_evento">
              <div class="cont">
                <a href="./event.php?evento=<?php echo $importantEvent["Id"];?>">
                  <div class="row event_image_row">
                    <div class="col">
                      <img class="event_image" src="<?php echo $importantEvent["Immagine"];?>"
                           alt="">
                    </div>
                  </div>
                  <div class="row event_date_row">
                    <div class="col">
                      <h6>
                        <?php
                          $date = DateTime::createFromFormat('Y-m-d',
                                                              $importantEvent["DataInizio"]);
                          echo "$date";
                          //echo date("D d M",  $date -> getTimestamp());
                        ?>
                      </h6>
                    </div>
                  </div>
                  <div class="row event_name_row">
                    <div class="col event_name">
                      <?php
                        /*
                         * Nome dell'evento
                         */
                        echo $importantEvent["NomeEvento"];
                      ?>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          <?php
          endforeach;
        endif;
      ?>
    </div>

    <div class="row">
      <div class="col type_events text-center">
        <h2>Eventi più importanti della settimana</h2>
      </div>
    </div>
    <div class="row events_row justify-content-around">
      <?php
        if(isset($templateParams["hotEvents"])):
          foreach($templateParams["hotEvents"] as $importantEvent):
            ?>
            <div class="col-6 col-md-3 casella_evento">
              <div class="cont">
                <a href="./event.php?evento=<?php echo $importantEvent["Id"];?>">
                  <div class="row event_image_row">
                    <div class="col">
                      <img class="event_image" src="<?php echo $importantEvent["Immagine"];?>"
                           alt="">
                    </div>
                  </div>
                  <div class="row event_date_row">
                    <div class="col">
                      <h6>
                        <?php
                          $date = DateTime::createFromFormat('Y-m-d',
                                                             $importantEvent["DataInizio"]);
                          echo date("D d M",  $date -> getTimestamp());
                        ?>
                      </h6>
                    </div>
                  </div>
                  <div class="row event_name_row">
                    <div class="col event_name">
                      <?php
                        /*
                         * Nome dell'evento
                         */
                        echo $importantEvent["NomeEvento"];
                      ?>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          <?php
          endforeach;
        endif;
      ?>
    </div>
  </div>
</div>