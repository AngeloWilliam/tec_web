﻿			<div id="myCarousel" class="my-3 px-1 py-2 h-100 carousel slide" data-interval="0" data-wrap="false">
				<form action="./php/registerUpdateEvent.php" class="h-100 text-center" method="POST"
              id="form_evento" enctype="multipart/form-data">
					<!-- Wrapper for slides -->
					<div class="px-1 h-100 carousel-inner" role="listbox">
						<!-- INSERIMENTO NOME EVENTO -->
						<div class="h-100 carousel-item active">
							<div class="h-50 row">
								<div class="mb-3 col">
									<?php
										if(isset($_SESSION["alter_event"])){
											if($eventInfo[0]["DataInizio"] != $eventInfo[0]["DataFine"])
												echo '<h1 class="alter_event" id="alter_multiple">Dai un nome al tuo evento:</h1>';
											else
												echo '<h1 class="alter_event" id="alter_one">Dai un nome al tuo evento:</h1>';
										}
										else
											echo '<h1>Dai un nome al tuo evento:</h1>';
									?>
								</div>
							</div>
							<div class="h-50 row">
								<div class="col-12">
									<?php
										if(isset($_SESSION["alter_event"])){
											$input= '<input type="text" class="col-12 col-md-8" name="nomeEvento"
											id="nomeEvento" value="';
											$input = $input . $eventInfo[0]["NomeEvento"] . '" required>';
											echo $input;
										}
										else{
										echo '<input type="text" class="col-12 col-md-8" placeholder="Nome evento" name="nomeEvento"
											id="nomeEvento" required>';
										}
									?>
								</div>
							</div>
						</div>
						<!-- INSERIMENTO GIORNO DI INIZIO E DI FINE EVENTO-->
						<div class="carousel-item">
							<div class="row">
								<div class="mb-2 col">
									<h1>Definisci un periodo:</h1>
									<label class="seeInput"><input type="radio" name="period" id="radioOne" value="one" />Un giorno</label>
									<label class="seeInput"><input type="radio" name="period" id="radioMultiple" value="multiple" />Più giorni</label>
								</div>
								<div class="w-100"></div>
								<div class="col-12">
									<p>Inizio:</p>
									<?php
										if(isset($_SESSION["alter_event"])){
											$input = '<input type="date" class="col-12 col-md-8 text-center" id="inizioEvento" name="inizioEvento" value="' . $eventInfo[0]["DataInizio"] . '" required>';
											echo $input;
										}
										else{
											echo '<input type="date" class="col-12 col-md-8 text-center" id="inizioEvento" name="inizioEvento" required>';
										}
									?>
								</div>
								<div class="col-12" id="divHide" style="display: none;">
									<p>Fine:</p>
									<?php
										if(isset($_SESSION["alter_event"])){
											$input = '<input type="date" class="col-12 col-md-8 text-center" id="fineEvento" name="fineEvento" value="' . $eventInfo[0]["DataFine"] . '">';
											echo $input;
										}
										else{
											echo '<input type="date" class="col-12 col-md-8 text-center" id="fineEvento" name="fineEvento">';
										}
									?>
								</div>
							</div>
						</div>
						<!-- INSERIMENTO LUOGO EVENTO -->
						<div class="h-100 carousel-item">
							<div class="h-50 row">
								<div class="mb-3 col">
									<h1>In che luogo si terrà?</h1>
								</div>
							</div>
							<div class="h-50 row">
								<div class="col-12">
									<?php
										if(isset($_SESSION["alter_event"])){
											echo '<input type="text" class="col-12 col-md-8" placeholder="Luogo" name="luogoEvento" id="luogoEvento" value="' . $eventInfo[0]["Luogo"] . '" required>';
										}
										else{
											echo '<input type="text" class="col-12 col-md-8" placeholder="Luogo" name="luogoEvento" id="luogoEvento" required>';
										}
									?>
								</div>
							</div>
						</div>
						<!-- INSERIMENTO DESCRIZIONE EVENTO -->
						<div class="h-100 carousel-item">
							<div class="h-50 row">
								<div class="col">
									<h1>Descrizione</h1>
									<p>Ricordati di inserire l'orario</p>
								</div>
							</div>
							<div class="h-50 row">
								<div class="col">
									<?php
										if(isset($_SESSION["alter_event"])){
											echo '<textarea rows="4" cols="30" name="comment" id="comment" form="form_evento" placeholder="Descrizione" required>' . $eventInfo[0]["Descrizione"] . '</textarea>';
										}
										else{
											echo '<textarea rows="4" cols="30" name="comment" id="comment" form="form_evento" placeholder="Descrizione" required></textarea>';
										}
									?>
								</div>
							</div>
						</div>
						<!-- INSERIMENTO IMMAGINE EVENTO -->
						<div class="h-100 carousel-item">
							<div class="h-25 row">
								<div class="col">
									<h1>Per ultima cosa, carica una foto!</h1>
									<p>Possibilmente in proporzione 16:9</p>
								</div>
								<div class="col">
									
										<?php
											if(!isset($_SESSION["alter_event"])){
												echo '<div class="col badge badge-secondary text-wrap">
														<label class="seeInput"><input multiple type="file" class="custom-file-input" id="fileName[]" name="fileName[]" required>Seleziona le immagini...</label>
													</div>';
											}
										?>
									
								</div>
							</div>
							<div class="row" id="imageCont">
								<?php/*
									foreach($eventImages as $img){
										echo '<div class="col-sm">
													<img class="w-100" src="' . IMG_DIR . $img[0]["Immagine"] . '">
												</div>';
									
									echo '<div class="col-6 col-md-3 casella_evento">
												<div class="row event_image_row">
													<div class="col">
														  <img class="event_image" src="' . IMG_DIR . $img[0]["Immagine"] . '"  alt="">
													</div>
											  </div>
										</div>';
									}*/
								?>
							</div>
						</div>
						<!-- FINE INSERIMENTO IMMAGINE -->
					</div>
					<!-- FINE CAROUSEL INNER -->
				</form>
					<!-- FINE MYCAROUSEL -->
			</div>
			<div class="my-3 h-100 row justify-content-around">
				<div class="col-4">
					<button href="#myCarousel" class="btn btn-primary btn-sm" role="button" data-slide="prev">prev</button>
				</div>
				<div class="col-4">
					<button type="submit" class="btn btn-primary btn-sm" form="form_evento" id="btnSubmit" name="submit">
					<?php
						if(isset($_SESSION["alter_event"])){
							echo "MODIFICA";
						}
						else{
							echo "CREA";
						}
					?>
					</button>
				</div>
				<div class="col-4">
					<button href="#myCarousel" class="btn btn-primary btn-sm" role="button"
                  data-slide="next">next</button>
				</div>
			</div>

