          <div class="row">
            <div class="col-12">
              <h1>Carrello</h1>
            </div>
            <!--            --><?php
              //            InfoPrinting::print_preformatted_array_info("\$templateParams",
              //                                                        $templateParams);
              //            InfoPrinting::print_preformatted_array_info("\$_SESSION", $_SESSION);
              //
              //            ?>
          </div>
          <div class="row">
            <div class="col-12 py-1">
              <?php
                foreach($templateParams["listaCarrello"] as $id_array => $acquisto):
                  ?>
                  <div class="row px-0 align-items-center text-left text-dark" id="biglietto<?=
                    $acquisto["biglietto"]["Id"] ?>">

                    <!-- Colonna per immagine -->
                    <div class="col-sm-4">
                      <a href="event.php?evento=<?= $acquisto["evento"]["Id"] ?>"
                         class="text-decoration-none">
                        <img src="<?= $acquisto["evento"]["Immagine"] ?>" alt=""
                             class="event_image">
                      </a>
                    </div>

                    <!-- Colonna per dati evento -->
                    <div class="col-sm-4 pl-sm-0">
                      <p> <?= $acquisto["evento"]["NomeEvento"] ?> </p>
                      <p> <?= $acquisto["evento"]["DataInizio"] ?> </p>
                      <p class="font-italic"> Luogo: <?= $acquisto["evento"]["Luogo"] ?> </p>
                      <p> <?= $acquisto["biglietto"]["Nome"] ?></p>
                      <p> <?= $acquisto["biglietto"]["Prezzo"] . " €" ?></p>
                      <!--                      <button onclick="rimuoviBiglietti(-->
                      <!--                      --><?//= $acquisto["biglietto"]["Id"]
                      ?>
                      <!--                       )">-->
                      <!--                        Rimuovi-->
                      <!--                      </button>-->
                      <button type="button" class="btn btn-outline-secondary btn-sm"
                              onclick="rimuoviBiglietti(<?= $acquisto["biglietto"]["Id"] ?>)">
                        Rimuovi
                      </button>
                      <!--                      --><?//= InfoPrinting::print_preformatted_array_info("\$acquisto", $acquisto)
                      ?>
                    </div>

                    <div class="col-sm-4">
                      <p>Quantità</p>
                      <p> <?= $acquisto["biglietto"]["NelCarrello"] ?></p>
                    </div>


                  </div>

                <?php
                endforeach;
              ?>
              <hr class="hrstyle">
              <div class="row totale_riga">
                <div class="col-6 col-md-4 h3">
                  Totale:
                </div>
                <div class="col-6 col-md-2 h3 totale_valore">
                  <?php
                    echo $templateParams["totale"] . " €";
                  ?>
                </div>
                <div class="col-md-6">
                  <button type="button" class="btn btn-success" onclick="acquistoBiglietti()">Procedi al
                    pagamento!</button>
                </div>
              </div>
            </div>
          </div>