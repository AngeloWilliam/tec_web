﻿<div class="h-100 row justify-content-center align-items-center text-center">
	<div class="col-8">
		<div class="jumbotron">
			<div class="row">
				<div class="col">
					<h1>Manca un'ultima cosa!</h1>
					<h3>Devi registrare almeno un tipo di biglietto:</h3>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<form>
						<div class="row align-items-center">
							<div class="col-6">
								<label>Nome del biglietto:</label>
								<input type="text" class="w-75" name="nome" id="nome">
							</div>
							<div class="col">
								<label>Numero biglietti:</label>
								<input type="number" name="totbiglietti" class="w-50" pattern="[0-9]" title="Minimo 1 biglietto, massimo 999999 biglietti." id="totbiglietti" min="1" max="999999">
							</div>
							<div class="col-2 align-items-center">
								<label class="w-25">Prezzo:</label>
								<input type="number" class="w-25" name="prezzoeuro" pattern="[0-9]" title="Inserisci un prezzo, da 0,0 a 999,99 euro." id="prezzoeuro" min="0" max="999">
								,
								<input type="number" class="w-25" name="prezzocent" pattern="[0-9]" title="Inserisci un prezzo, da 0,0 a 999,99 euro." id="prezzocent" min="0" max="99">
							</div>
							<div class="col-1">
								<button class="btn btn-primary" type="button" id="btnRegister">Registra</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="pre-scrollable">
						<table class="table">
							<thead>
								<tr>
									<th scoper="col" class="col-7">Tipo Biglietto</th>
									<th scoper="col" class="text-center">Prezzo</th>
									<th scoper="col" class="text-center">Posti</th>
								</tr>
							</thead>
							<tbody id="tab_body">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<button class="btn btn-primary" type="button" id="btnDone">Fatto</button>
			</div>
		</div>
	</div>
</div>