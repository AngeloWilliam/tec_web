﻿ <div class="row justify-content-center">
                <div class="col-12">
					<div class="row">
						<?php
							echo '<div class="col-10"><h2>';
							switch($_SESSION["boardType"]){
								case "Utenti":
									echo 'Gestisci Utenti';
									break;
								case "Eventi":
									echo "Gestisci Eventi";
									break;
								case "Messaggi":
									echo "Notifiche";
									break;
							}
							echo '</h2></div>';
							if($_SESSION["tipoUtente"] == "Amministratori"){
								if($_SESSION["boardType"] == "Eventi"){
									echo '<div class="col-2"><select name="selectManage" id="selectManage">
										<option value="Eventi" selected="selected">Eventi</option>
										<option value="Utenti">Utenti</option>
									</select></div>';
								}
								else{
									echo '<div class="col-2"><select name="selectManage" id="selectManage">
										<option value="Eventi">Eventi</option>
										<option value="Utenti" selected="selected">Utenti</option>
									</select></div>';
								}
							}
						?>
					</div>
                    <div class="row">
                        <div class="col">
                            <button class="btn btn-primary float-left" type="button"
                                    id="btnBackward"><</button>
                        </div>
                        <div class="col">
                            <button class="btn btn-primary float-right" type="button"
                                    id="btnForward">></button>
                        </div>
                    </div>
                    <div class="col-12" id="listContainer">
                    </div>
                </div>
            </div>
