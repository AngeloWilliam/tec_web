<section class="event">

  <div class="container-fluid">
    <div class="row no-gutters" id="main_content_row" style="width: 100%;">
      <div class="col-lg-auto col-xl-8" id="colonna_jumbotron">
        <div class="jumbotron mycustom-jumbotron">

          <div class="row">
            <div class="col-12 collonna_titolo">
              <h1 class="titolo_evento">
                Biglietti disponibili
              </h1>
              <hr class="hrstyle">
            </div>
          </div>


          <div class="row justify-content-around">
            <div class="col-sm-6 text-center">
              <select class="form-control" id="ticketsCost" onchange="prova()">
                <?php
                  foreach($templateParams["Tickets"] as $ticket){
                    $disabled = "";
                    if($ticket["NumVenduti"] >= $ticket["TotBiglietti"]){
                      $disabled = " disabled";
                    }
                    echo '<option value="'. $ticket["Prezzo"] .'|'. $ticket["Id"] . '"' .
                      $disabled . '>' .
                      $ticket["Nome"] .
                      ' - ' . $ticket["Prezzo"]
                      .' €</option>';
                  }
                ?>
              </select>
            </div>
            <div class="col-sm-6 text-center">
              <select class="form-control" onchange="prova()" id="ticketsQuantity">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </select>
            </div>
            <div class="col-12" id="totale">
            </div>
            <div class="12">
              <?= InfoPrinting::print_preformatted_array_info("Template Params", $templateParams)?>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>

</section>