﻿<h1 id="zone_title">Registrazione</h1>

<form onsubmit="return registrazione('<?= $templateParams['tipoRegistrazione'] ?>')"
      action="#" method="POST" id="input_data_account" enctype="multipart/form-data">

  <label for="usname">
    <b>Username</b>
  </label>
  <input type="text" placeholder="Inserire username" name="usname"
         id="usname"
         required="">

  <label for="email">
    <b>Indirizzo mail</b>
  </label>
  <input type="email" placeholder="Inserire indirizzo mail" name="email"
         id="email"
         required="">

  <?php
    if($templateParams['tipoRegistrazione'] == 'Organizzatori'):
      ?>
      <label for="name">
        <b>Nome</b>
      </label>
      <input type="text" placeholder="Inserire nome" name="name"
             id="name"
             required="">

      <label for="surname">
        <b>Nome</b>
      </label>
      <input type="text" placeholder="Inserire cognome" name="surname"
             id="surname"
             required="">

      <div class="miaom">

        <label for="piva">
          <b>Indirizzo mail</b>
        </label>
        <input type="number" placeholder="Inserire codice partita IVA" name="piva"
               minlength="11" maxlength="11"
               id="piva"
               required="">
      </div>
    <?php
    endif;
  ?>

  <label for="psw">
    <b>Password</b>
  </label>
  <input type="password" placeholder="Inserire Password" name="psw" id="psw"
         required="">

  <label for="confirm_psw">
    <b>Conferma Password</b>
  </label>
  <input type="password" placeholder="Inserire Password Nuovamente" name="confirm_psw"
         id="confirm_psw" required="">


  <button type="submit" id="bottone_registrazione">Registrati!</button>
</form>
