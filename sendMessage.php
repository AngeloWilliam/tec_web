﻿<?php
	
	require_once "bootstrap.php";
	require_once "init_stylesheet_script.php";
	require_once "utils/init_navbar_footer.php";

	//Base Template
	$templateParams["title"] = "Events Finder - Invio Messaggio";
	$templateParams["pageRequested"] = "sendMessage-form.php";

	$templateParams["stylesheet"] = get_stylesheets_as_array();
	$templateParams["js"] = get_script_as_array();

	array_push($templateParams["js"], 'src="./javascript/sendMessage.js"');

	require 'template/base.php';
?>