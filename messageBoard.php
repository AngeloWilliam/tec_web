﻿<?php
	//La richiesta della pagina è iniziata richiamando questo file
	
	require_once "bootstrap.php";
	require_once "init_stylesheet_script.php";
	require_once "utils/init_navbar_footer.php";

	$_SESSION["boardType"]="Messaggi";

	//Cambio il titolo e richiedo la struttura della pagina
	//Base Template
	$templateParams["title"] = "Events Finder - Casella Messaggi";
	$templateParams["pageRequested"] = "board-form.php";

	$templateParams["stylesheet"] = get_stylesheets_as_array();
  array_push($templateParams["stylesheet"], 'rel = "stylesheet" href = "./css/profile.css"');
	$templateParams["js"] = get_script_as_array();

	/*	La struttura della pagina è una lista da riempire, includo
		innanzitutto lo script buildBoard.js che contiene i metodi per 
		riempire la lista (fisicamente) e poi richiamo lo script dedicato
		a questa pagina, in questo caso messageBoard.js
		Richiamando prima buildBoard faccio in modo che i suoi metodi possano essere
		utilizzati dagli script che includo successivamente (l'ordine è importante!)
	*/
	array_push($templateParams["js"], 'src="./javascript/buildBoard.js"');
	array_push($templateParams["js"], 'src="./javascript/messageBoard.js"');

	require 'template/base.php';
?>