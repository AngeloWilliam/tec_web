﻿<?php
	require_once('bootstrap.php');

	if(isset($_REQUEST["richiesta"])){
		switch($_REQUEST["richiesta"][0]){
			case "eventi_organizzatore":
				$result = $dbh->getEventiOrganizzatore($_SESSION["Id"], 0);
				break;
			case "ricercaEventi":
				$tmp_result = $dbh->searchEvento($_REQUEST["richiesta"][1], $_REQUEST["richiesta"][2], $_REQUEST["richiesta"][3]);
				$result = array();
				foreach($tmp_result as $event){
					$image_event = $dbh->getEventoImages($event["Id"])[0]["Immagine"];
					if(isset($image_event)) {
						$event["Immagine"] = IMG_DIR.$image_event;
					}
					array_push($result, $event);
				}
				break;
			case "ricercaPresel":
				if($_REQUEST["richiesta"][1] == "name") {
					$tmp_result = $dbh->searchEvento($_REQUEST["richiesta"][2], "", "");
				} else if ($_REQUEST["richiesta"][1] == "place") {
					$tmp_result = $dbh->searchEvento("",$_REQUEST["richiesta"][2], "");
				}
				$result = array();
				foreach($tmp_result as $event){
					$image_event = $dbh->getEventoImages($event["Id"])[0]["Immagine"];
					if(isset($image_event)) {
						$event["Immagine"] = IMG_DIR.$image_event;
					}
					array_push($result, $event);
				}
				break;
			case "tipobiglietto":
				$result = $dbh->registerTipoBiglietto($_SESSION["IdEventoBiglietto"],$_SESSION["Id"],$_REQUEST["richiesta"][1], $_REQUEST["richiesta"][2],$_REQUEST["richiesta"][3]);
				break;
			case "messaggio":
				if($_SESSION["tipoUtente"]=="Organizzatori")
					$tipodestinatario="Clienti";
				else
					$tipodestinatario="Organizzatori";

				$result = $dbh->invioMessaggio($_SESSION["Id"], $_SESSION["tipoUtente"], $tipodestinatario,	$_REQUEST["richiesta"][1], $_REQUEST["richiesta"][2] , $_REQUEST["richiesta"][3]);
				break;
			case "lista_messaggi" :
				if($_SESSION["tipoUtente"]=="Organizzatori")
					$result = $dbh->getMessagesOrganizzatori($_SESSION["Id"],$_REQUEST["richiesta"][1]);
				else
					$result = $dbh->getMessages($_SESSION["Id"],$_REQUEST["richiesta"][1]);
				break;
			case "lista_utenti"	:
				$result = $dbh->getListaUtenti("Utenti",$_REQUEST["richiesta"][1]);
				$_SESSION["boardType"]="Utenti";
				break;
			case "lista_eventi"	:
				if($_SESSION["tipoUtente"]=="Organizzatori"){
					$tmp_result = $dbh->getEventiOrganizzatore($_SESSION["Id"], $_REQUEST["richiesta"][1]);
					$result = array();
					foreach($tmp_result as $event){
						$image_event = $dbh->getEventoImages($event["Id"])[0]["Immagine"];
						if(isset($image_event)) {
							$event["Immagine"] = IMG_DIR.$image_event;
						}
						array_push($result, $event);
					}
				}
				else{
					$result = $dbh->getEventiAdmin($_REQUEST["richiesta"][1]);
					$_SESSION["boardType"]="Eventi";
				}
				break;
			case "notifica_letta":
				$result = $dbh->setNotificationRead($_SESSION["Id"],$_REQUEST["richiesta"][1]);
				break;
			case "elimina_utente":
				$dbh->deleteUser($_REQUEST["richiesta"][1]);
				break;
			case "elimina_evento":
				$result = $dbh->deleteImmaginiEvento($_REQUEST["richiesta"][1], "ALL");
				if($result == 1){
					$result = $dbh->deleteEvento($_REQUEST["richiesta"][1]);
				}
				
				break;
			case "img_alter":
				$result = $dbh->getEventoImages($_SESSION["alter_event"][0]);
				break;
		}		
		
		if($_REQUEST["richiesta"][0] == "lista_messaggi" || $_REQUEST["richiesta"][0] == "lista_utenti" || $_REQUEST["richiesta"][0] == "lista_eventi"){
			if(count($result) > 10){
				array_pop($items);
				$anotherPage = true;
			}
			else
				$anotherPage = false;

			array_push($result, $anotherPage);
			array_push($result, $_SESSION["boardType"]);
		}
	}
	elseif(isset($_POST["richiesta"])){
		switch($_POST["richiesta"][0]){
			case "registra_evento":
				//$errore = false;
				//$imageList = rearrangeUpload($_FILES["fileName"]);
				/*
				$idevento = $dbh -> registerEvento($_SESSION["Id"], $_POST["richiesta"]["nome_evento"], $_POST["richiesta"]["luogo_evento"], $_POST["richiesta"]["inizio_evento"], $_POST["richiesta"]["fine_evento"], $_POST["richiesta"]["comment"]);
				$namelist = array();
				foreach($imageList as $img){
					list($result, $msg) = uploadImage("./.".IMG_DIR, $img);
					if($result < 1){
						$templateParams["errore"] = "Upload immagine fallito, " + $msg;
						$errore = true;
					}
					else{
						array_push($namelist,$msg);
					}
				}
				if(!$errore){
					$result = $dbh->uploadImmaginiEvento($idevento, $namelist);
					$_SESSION["IdEventoBiglietto"] = $idevento;
					require 'titcketRegister.php';
				}*/
				//$result = $_FILES["fileName"];
				break;
			case "tipobiglietto":
				$result = $dbh->registerTipoBiglietto($_SESSION["IdEventoBiglietto"],$_SESSION["Id"],$_POST["richiesta"][1], $_POST["richiesta"][2],$_POST["richiesta"][3]);
				break;
		}
	}
	else{
		$result = array($_REQUEST, $_POST);
	}

	header('Content-Type: application/json');
	echo json_encode($result);
?>