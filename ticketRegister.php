﻿<?php

	require_once "bootstrap.php";
	require_once "init_stylesheet_script.php";
	require_once "utils/init_navbar_footer.php";

    $templateParams["stylesheet"] = get_stylesheets_as_array();
    array_push($templateParams["stylesheet"], 'rel = "stylesheet" href = "./css/event.css"');
    $templateParams["js"] = get_script_as_array();

    $templateParams["pageRequested"] = "ticketRegister-form.php";
	array_push($templateParams["js"], 'src="./javascript/ticketRegister.js"');



	require 'template/base.php';
?>