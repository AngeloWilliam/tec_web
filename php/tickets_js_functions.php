<?php

  header('Content-Type: application/json');
  include "../utils/functions_angelo.php";
  require_once "./../bootstrap.php";
  $aResult = array();
  function are_there_arguments() {

    return isset($_POST['arguments']);
  }


  if(!isset($_POST['functionname'])) {
    $aResult['error'] = 'No function name!';
  } else {

    switch($_POST['functionname']) {
      case 'delete_tickets':
        if(are_there_arguments() && count($_POST['arguments']) != 1) {
          $aResult['error'] = 'Error in arguments!';
        } else {
          $aResult['result'] = true;
          $aResult['preSession'] = $_SESSION["Chart"];
//            $valore_da_rimuovere = $_SESSION["Chart"][$_POST["arguments"]]["Totale"];
          unset($_SESSION["Chart"][$_POST['arguments']]);
          $aResult['postSession'] = $_SESSION["Chart"];
          $aResult['session'] = $_SESSION["Chart"];
          $aResult['totale'] = 0;
          foreach($_SESSION["Chart"] as $item_carrello) {
            $aResult['totale'] += $item_carrello["Totale"];
          }
        }
        break;
      case 'are_there_tickets':
        $aResult["areThereTickets"] = count($_SESSION["Chart"]) >= 1 ? true : false;
        break;
      case 'buy_tickets':
//            $aResult['biglietti'] = array();
        $aResult['debug'] = "";
        $aResult['itemPassed'] = array();
        $aResult['bigliettiDisponibili'] = true;
        foreach($_SESSION["Chart"] as $id_biglietto => $item_carrello) {
          //$aResult['nonDisponibili'] =
          $biglietto_da_passare = $dbh -> getSingoloBiglietto($id_biglietto)[0];
          $evento_da_passare = $dbh -> getSingoloEvento($item_carrello['IDEvento'])[0];
          $aResult['debug'] = $aResult['debug'] . "Biglietto da passare: " . print_r
            ($biglietto_da_passare, true);
          $aResult['debug'] = $aResult['debug'] . "Evento da passare: " . print_r
            ($evento_da_passare, true);
          $item_da_passare = array();
          $item_da_passare['biglietto'] = $biglietto_da_passare;
          $item_da_passare['evento'] = $evento_da_passare;
          if($biglietto_da_passare['NumVenduti'] + $item_carrello['NumeroBiglietti'] >
            $biglietto_da_passare['TotBiglietti']) {
            $item_da_passare['venduto'] = false;
            $aResult['bigliettiDisponibili'] = false;
          } else {
            $item_da_passare['venduto'] = true;
          }
          array_push($aResult['itemPassed'], $item_da_passare);
        }
        if($aResult['bigliettiDisponibili']) {
          foreach($_SESSION["Chart"] as $id_biglietto => $item_carrello) {
            $acquisto_buon_fine = $dbh -> acquistoBiglietti($_SESSION["Id"], $id_biglietto,
                                                            $item_carrello["IDEvento"],
                                                            $item_carrello["NumeroBiglietti"]);
            $aResult['debug'] =
              $aResult['debug'] . "Appena acquistato biglietto con ID: $id_biglietto"
              . " esito : " . $acquisto_buon_fine . "\n";
            if($acquisto_buon_fine < 0) {
              $aResult['bigliettiDisponibili'] = false;
              $aResult['debug'] =
                $aResult['debug'] . "Biglietto con ID: $id_biglietto non acquistato. \n";
            } else {
              $aResult['debug'] =
                $aResult['debug'] . "Biglietto con ID: $id_biglietto acquistato. \n";
            }
          }

          if($aResult['bigliettiDisponibili']){
            unset($_SESSION["Chart"]);
          }
        }
        break;

      case 'register_utente':
        if(!isset($_POST['arguments'])){
          $aResult['error'] = 'Error in arguments!';
        } else {
          $username = $_POST['arguments']['username'];
          $name = $_POST['arguments']['name'];
          $surname = $_POST['arguments']['surname'];
          $mail = $_POST['arguments']['mail'];
          $piva = $_POST['arguments']['piva'];
          $psw = $_POST['arguments']['psw'];
          $conf_psw = $_POST['arguments']['confPsw'];
          $tipologia_utente = $_POST['arguments']['tipologiaUtente'];
          $aResult['debug'] .= "\$_POST array => \n" . print_r($_POST, true) . "\n\n";
          $aResult['debug'] .= "usname = $username \n";
          $aResult['debug'] .= "name = $name \n";
          $aResult['debug'] .= "surname = $surname \n";
          $aResult['debug'] .= "mail = $mail \n";
          $aResult['debug'] .= "piva = $piva \n";
          $aResult['debug'] .= "psw = $psw \n";
          $aResult['debug'] .= "confPsw = $conf_psw \n";
          $aResult['debug'] .= "tipologiaUtente = $tipologia_utente \n";

          if($tipologia_utente == 'Clienti'){
            $aResult['debug'] .= "Procedo alla registrazione cliente\n";
            $esito_registrazione = $dbh -> registerUtente('Clienti', $username, $mail, $psw, "", "", "");
          } else {
            $esito_registrazione = $dbh -> registerUtente('Organizzatori', $username, $mail, $psw, $name, $surname, $piva);
            $aResult['debug'] .= "Procedo alla registrazione organizzatore\n";
          }
          $aResult['debug'] .= print_r($esito_registrazione, true);
          if($esito_registrazione['errno'] == 1062){
            $aResult['debug'] .= "L'indirizzo mail è già presente\n";
            $aResult['ripetizione'] = true;
          } elseif($esito_registrazione[0] == -1){
            $aResult['debug'] .= "La registrazione non è andata a buon fine\n";
            $aResult['error'] = 'La registrazione non è andata a buon fine';
          } else {
            $aResult['debug'] .= "Registrazione andata a buonfine\n";
            $aResult['debug'] .= "\$esito_registrazione[0] = " .$esito_registrazione[0] . "\n";
            $aResult['debug'] .= "\$tipologia_utente = " .$tipologia_utente . "\n";
            $utente_registrato = $dbh -> getUtente($esito_registrazione[0], $tipologia_utente)[0];
            $utente_registrato['TipoUtente'] = $tipologia_utente;
            $aResult['debug'] .= "Utente registrato: \n";
            $aResult['debug'] .= print_r($utente_registrato, true);
            registerLoggedUser($utente_registrato);
            $aResult['debug'] .= "\$_SESSION attuale: \n";
            $aResult['debug'] .= print_r($_SESSION, true);

          }
        }

        break;

      default:
        $aResult['error'] = 'Not found function ' . $_POST['functionname'] . '!';
        break;
    }

  }
  echo json_encode($aResult);
?>

