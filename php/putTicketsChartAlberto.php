﻿<?php
	
	if(isset($_GET["IDBiglietto"]) && isset($_GET["IDEvento"]) && isset($_GET["NumeroBiglietti"])) {
		require_once "./../bootstrap.php";

		$idEvento = $_REQUEST["acquistoBiglietti"][0];
		$idBiglietto = $_REQUEST["acquistoBiglietti"][1];
		$numBiglietti = $_REQUEST["acquistoBiglietti"][2];

		$biglietti_evento = $dbh -> getBigliettiEvento($idEvento);
		$biglietti_disponibili = 0;
		$esito = array();

		if(isset($_SESSION["Chart"]) && isset($_SESSION["Chart"][$idBiglietto])) {
			$biglietti_nel_carrello = $_SESSION["Chart"][$idBiglietto]][$numBiglietti];
		} else {
			$biglietti_nel_carrello = 0;
		}
		$_SESSION["biglietti_nel_carrello"] = $biglietti_nel_carrello;
		
		foreach($biglietti_evento as $tipo_biglietto) {
			if($tipo_biglietto["Id"] == $idBiglietto) {
				$biglietti_disponibili = $tipo_biglietto["TotBiglietti"] - $tipo_biglietto["NumVenduti"];
				if($biglietti_disponibili >= ($numBiglietti + $biglietti_nel_carrello)) {
					if(!isset($_SESSION["Chart"])) {
						$_SESSION["Chart"] = array();
					}
					if(!isset($_SESSION["Chart"][$idBiglietto])) {
						$_SESSION["Chart"][$idBiglietto] = array(
						"IDEvento" => $idEvento,
						"NumeroBiglietti" => $numBiglietti);
					} else {
						$_SESSION["Chart"][$idBiglietto][$idEvento] = $idEvento;
						$_SESSION["Chart"][$idBiglietto][$numBiglietti] += $numBiglietti;
					}

					$esito["inserimentoOk"] = true;
					$esito["bigliettiCarrello"] = $_SESSION["Chart"][$idBiglietto][$numBiglietti];

				} else {
					$esito["inserimentoOk"] = false;
					$esito["bigliettiCarrello"] = $biglietti_nel_carrello;
				}

				$esito["bigliettiNonVenduti"]=$biglietti_disponibili;
			}
		}

		header('Content-Type: application/json');
		echo json_encode($esito);
	}

?>
