﻿<?php

	require_once "./../bootstrap.php";
	require_once "./../init_stylesheet_script.php";
	require_once "./../utils/init_navbar_footer.php";

	$templateParams["stylesheet"] = get_stylesheets_as_array();
    $templateParams["js"] = get_script_as_array();

    if($_SERVER['REQUEST_METHOD'] == 'POST') {
		$errore = false;
        $imageList = rearrangeUpload($_FILES["fileName"]);
		$finevento = "aaa";
		
		if($_POST["fineEvento"] == ""){
			$finevento = $_POST["inizioEvento"];
		}
		else{
			$finevento = $_POST["fineEvento"];
		}
		
		

		if(!isset($_SESSION["alter_event"])){
			$idevento = $dbh -> registerEvento($_SESSION["Id"], $_POST["nomeEvento"], $_POST["luogoEvento"], $_POST["inizioEvento"], $finevento, $_POST["comment"]);
			$namelist = array();
			foreach($imageList as $img){
				list($result, $msg) = uploadImage("./.".IMG_DIR, $img);
				if($result < 1){
					$templateParams["errore"] = "Upload immagine fallito, " + $msg;
					$errore = true;
				}
				else{
					array_push($namelist,$msg);
				}
			}
			if(!$errore){
				$result = $dbh->uploadImmaginiEvento($idevento, $namelist);
				$_SESSION["IdEventoBiglietto"] = $idevento;
				header('Location: http://eventsfinder.altervista.org/ticketRegister.php');
			}
		}
		else{
			$result = $dbh->updateEvento($_SESSION["alter_event"], $_POST["nomeEvento"], $_POST["luogoEvento"], $_POST["inizioEvento"], $finevento, $_POST["comment"]);
			unset($_SESSION["alter_event"]);
			header('Location: http://eventsfinder.altervista.org/index.php');
		}
	}
?>