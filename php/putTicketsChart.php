<?php

  if(isset($_GET["IDBiglietto"]) && isset($_GET["IDEvento"]) && isset($_GET["NumeroBiglietti"])) {
    require_once "./../bootstrap.php";
//    echo "inside puttickets </br>";
    $biglietti_evento = $dbh -> getBigliettiEvento($_GET["IDEvento"]);
    $biglietti_disponibili = 0;
    $esito = array();

    if(isset($_SESSION["Chart"]) && isset($_SESSION["Chart"][$_GET["IDBiglietto"]])) {
      $biglietti_nel_carrello = $_SESSION["Chart"][$_GET["IDBiglietto"]]["NumeroBiglietti"];
    } else {
      $biglietti_nel_carrello = 0;
    }
    $_SESSION["biglietti_nel_carrello"] = $biglietti_nel_carrello;
    foreach($biglietti_evento as $tipo_biglietto) {
//      echo "inside foreach puttickets </br>";
      if($tipo_biglietto["Id"] == $_GET["IDBiglietto"]) {
//        echo 'inside if($tipo_biglietto["Id"] == $_GET["IDBiglietto"]) </br>';
        $biglietti_disponibili = $tipo_biglietto["TotBiglietti"] - $tipo_biglietto["NumVenduti"];
        if($biglietti_disponibili >= ($_GET["NumeroBiglietti"] + $biglietti_nel_carrello)) {

          if(!isset($_SESSION["Chart"])) {
//            echo "inside if 1</br>";
            $_SESSION["Chart"] = array();
          }

          if(!isset($_SESSION["Chart"][$_GET["IDBiglietto"]])) {
//            echo "inside if 2</br>";
            $_SESSION["Chart"][$_GET["IDBiglietto"]] = array(
//              "IDBiglietto" => $_GET["IDBiglietto"],
              "IDEvento" => $_GET["IDEvento"],
              "NumeroBiglietti" => $_GET["NumeroBiglietti"],
              "Totale" => $_GET["NumeroBiglietti"]*$tipo_biglietto["Prezzo"]
            );

          } else {
//            echo "inside if 3</br>";
            $_SESSION["Chart"][$_GET["IDBiglietto"]]["IDEvento"] = $_GET["IDEvento"];
            $_SESSION["Chart"][$_GET["IDBiglietto"]]["NumeroBiglietti"] += $_GET["NumeroBiglietti"];
            $_SESSION["Chart"][$_GET["IDBiglietto"]]["Totale"] =
              $_SESSION["Chart"][$_GET["IDBiglietto"]]["NumeroBiglietti"]*$tipo_biglietto["Prezzo"];
          }
          $esito["inserimentoOk"] = true;
          $esito["bigliettiCarrello"] = $_SESSION["Chart"][$_GET["IDBiglietto"]]["NumeroBiglietti"];
        } else {
          $esito["inserimentoOk"] = false;
          $esito["bigliettiCarrello"] = $biglietti_nel_carrello;
        }
        $esito["bigliettiNonVenduti"]=$biglietti_disponibili;
      }
    }
    $biglietti_nel_carrello = $_SESSION["Chart"][$_GET["IDBiglietto"]]["NumeroBiglietti"];
    echo json_encode($esito);
  }

