<!-- PAGINA RICOPIATA DA MESSAGE BOX. ADATTARE A CARRELLO -->
<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--<link rel="stylesheet" href="./../homepage/css/homepage.css">-->
    <link rel="stylesheet" href="./../componentspage/footer/css/footer.css">
    <link rel="stylesheet" href="./css/profile.css">
    <link rel="stylesheet" href="./css/messageBox.css">
    <script src="./../homepage/javascript/homepage.js"></script>
    <title>Events Finder</title>

    <?php
      require_once("./../bootstrap.php");
      $res = $dbh -> checkNotificationsPROVANGELO(5, "Clienti");
    ?>
  </head>

  <!--<style>
    #luogo_eventi {
      background-image: url("Homepage/Sfondi/background.png");
      background-color: #cccccc;
      background-size: cover;
    }
  </style>-->

  <body>
    <section id="nav">
      <?php include("./../componentspage/navbar/navbarNotLogged.html") ?>
    </section>

    <section class="profile">

      <div class="jumbotron mycustom-jumbotron">
        <div class="container-fluid">

          <div class="row messaggi">
            <div class="col-12 messaggi">
              <h1>Messaggi</h1>
            </div>
          </div>

          <?php
            $notifications_number = count($res) - 1;
            foreach($res as $v) {
              ?>

              <div class="row messaggio">
                <div class="col-12">

                  <!-- RIGA PER OGGETTO E LOCAZIONE TEMPORALE DELL'INVIO -->
                  <div class="row">
                    <div class="col-3">
                      Oggetto: <?php echo $v["Oggetto"] ?>
                    </div>

                    <div class="col-9">
                      <?php echo $v["DataInvio"] ?>
                    </div>
                  </div>

                  <!-- RIGA TESTO MESSAGGIO -->
                  <div class="row">
                    <div class="col-12">
                      <? echo $v["Testo"] ?>
                    </div>
                  </div>

                  <?
                    if($notifications_number > 0) {
                      ?>
                      <div class="row separatore">
                        <div class="col-12">
                          <hr class="hrstyle">
                        </div>
                      </div>
                      <?
                    }
                  ?>

                </div>
              </div>

              <?php
              $notifications_number--;
            }
          ?>
        </div>
      </div>
    </section>


    <section class="footer">
      <?php include("./../componentspage/footer/footer.html") ?>
    </section>
  </body>

</html>