﻿<div class="h-100 row justify-content-center align-items-center">
	<div class="col-10">
		<div class="jumbotron">
			<div class="row justify-content-left">
                <div class="col-sm">
                    <label id="loggedIn"><?php 
                            if($_GET["place"]){
                                echo 'place=' . $_GET["place"];    
                            }
                            else{
                                echo 'name=' . $_GET["name"];  
                            }
                        ?></label>
				</div>
				<div class="col-sm">
					<input name="nomeEvento" id="nomeEvento" placeholder="Nome">
				</div>
				<div class="col-sm">
					<input name="luogoEvento" id="luogoEvento" placeholder="Luogo">
				</div>
				<div class="col-sm">
					<input type="date" id="dataEvento" name="dataEvento">
				</div>
				<div class="col-sm">
					<button class="button" id="btnCerca" name="btnCerca">Cerca</button>
				</div>
			</div>
			<div class="pre-scrollable">
				<div class="row" id="eventList">
				</div>
			</div>
		</div>
	</div>
</div>