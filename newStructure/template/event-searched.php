<section id="searchedEvents">

  <div class="container-fluid">
    <!--justify-content-center align-items-center"-->
    <div class="row" id="main_content_row">

      <!--   PER DEBUG   -->

      <!--      <div class="col-12">-->
      <!--        --><? //
        //          InfoPrinting ::print_preformatted_array_info("Inside \$templateParams[\"eventsSearched\"]",
        //                                                       $templateParams["eventsSearched"]);
        //        ?>
      <!--      </div>-->


      <div class="col-lg-auto col-xl-9" id="colonna_jumbotron">
        <div id="jumbotron" class="jumbotron mycustom-jumbotron">
          <div class="row">
            <div class="col-12">
              <h1>Eventi</h1>
            </div>
          </div>
          <?php
            foreach($templateParams["eventsSearched"] as $key => $event):
              if($key === 0 || $key % 2 === 0) {
                echo '<div class="row">';
              }
              ?>
              <div class="col-md-6 py-1">
                <a href="event.php?evento=<?= $event["Id"] ?>" class="text-decoration-none">
                  <div class="row px-0 align-items-center text-left text-dark">

                    <!-- Colonna per immagine -->
                    <div class="col-sm-7">
                      <img src="<?= $event["Immagine"] ?>" alt="" class="event_image">
                    </div>

                    <!-- Colonna per dati evento -->
                    <div class="col-sm-5 pl-sm-0">
                      <p> <?= $event["NomeEvento"] ?> </p>
                      <p> <?= $event["DataInizio"] ?> </p>
                      <p class="font-italic"> Luogo: <?= $event["Luogo"] ?> </p>
                    </div>
                  </div>
                </a>
              </div>
              <?php
              if($key % 2 === 1 || ($key + 1) === count($templateParams["eventsSearched"])) {
                echo '</div>';
              }
            endforeach;
          ?>
        </div>
      </div>
    </div>
  </div>

</section>