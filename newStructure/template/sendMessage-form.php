﻿<div class="h-100 row justify-content-center align-items-center">
	<div class="col-8">
		<div class="jumbotron">
			<div class="row">
				<div class="col">
					<h2>Invio Messaggio</h2>
				</div>
				<div class="col-12">
					<?php
						if($_SESSION["tipoUtente"]=="Organizzatori")
							echo "<h5>Scegli ai partecipanti di quale evento inviare il messaggio:</h5>";
						else
							echo "<h5>Invia un messaggio agli organizzatori:</h5>";
					?>
				</div>
			</div>
			<?php if($_SESSION["tipoUtente"]=="Organizzatori"): ?>
				<div class="row">
					<div class="col">
						<div class="form-group">
							<select class="form-control" id="listEvent">
							</select>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-12" id="input-group">
					<textarea class="form-control w-100" rows="1" id="oggetto" maxlength="20" name="oggetto" placeholder="Oggetto"></textarea>
					<textarea class="form-control w-100" rows="5" id="messaggio" maxlength="5000" name="messaggio" placeholder="Scrivi qui il tuo messaggio."></textarea>
				</div>
				<div class="col">
					<button class="btn btn-primary" type="button" id="btnSend">Invia</button>
				</div>
			</div>
		</div>
	</div>
</div>