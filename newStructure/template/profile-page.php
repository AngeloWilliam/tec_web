<section class="profile">

  <div class="container-fluid">
    <div class="row" id="main_content_row">
      <div class="col-lg-8 col-xl-auto">

        <div id="jumbotron" class="jumbotron mycustom-jumbotron">

          <div class="row title">
            <div class="col-12 text-center">
              <?php
                echo "<h1>Bentornato " . $_SESSION["NomeUtente"] . "!</h1>";
              ?>
            </div>
          </div>

          <div class="row riga_dati_utente">
            <?php
              if($_SESSION['tipoUtente'] == 'Clienti'){
            ?>
            <div class="col-lg-4 dati_utente">
              <?php
                } else {
              ?>
              <div class="col-lg-12 dati_utente">
                <?php
                  }
                ?>
                <p>
                <span>
                  Nome utente
                </span>
                  <br/>
                  <?php
                    echo $_SESSION["NomeUtente"];
                  ?>
                </p>
                <p>
                  <span>Indirizzo mail</span>
                  <br/>
                  <?php
                    echo $_SESSION["Mail"];
                  ?>
                </p>
                <p>
                  <a href="#">Desidero cambiare password</a>
                </p>
                <?php
                  if($_SESSION["tipoUtente"] != "Amministratori") {
                    echo '<p>
							<a href="messageBoard.php">Mail box</a>
						  </p>';
                  }
                ?>
                <?php
                  if($_SESSION["tipoUtente"] == "Organizzatori") {
                    echo '<p>
							<a href="sendMessage.php">Invia messaggio</a>
						  </p>
						  <p>
							<a href="event_creation.php">Crea evento</a>
						  </p>';
                  } elseif($_SESSION["tipoUtente"] == "Amministratori") {
                    echo '<p>
							<a href="sendMessage.php">Invia messaggio</a>
						  </p>
						  <p>
							<a href="adminBoard_Users.php">Amministrazione</a>
						  </p>';
                  }
                ?>
              </div>

              <!-- colonna per eventi in programma -->
              <?php
                if($_SESSION['tipoUtente'] == 'Clienti') {
                  ?>
                  <div class="col-lg-8">

                    <h2 class="text-center"> Eventi in programma</h2>

                    <?php
                      foreach($templateParams['eventi'] as $evento_in_programma):
                        ?>
                        <div class="row event">
                          <div class="col-2 px-0 mx-0">
                            <img src="<?= $evento_in_programma['Immagine'] ?>" alt="event_logo">
                          </div>
                          <!--
                          Nome, data
                          -->
                          <div class="col-8 px-0 mx-0">

                            <div class="row px-0 mx-0">
                              <div class="col-12 px-0 mx-0">
                                <?= $evento_in_programma['NomeEvento'] ?>
                              </div>
                            </div>

                            <div class="row px-0 mx-0">
                              <div class="col-12 px-0 mx-0">
                                <?= $evento_in_programma['DataInizio'] ?>
                              </div>
                            </div>

                            <div class="row px-0 mx-0">
                              <div class="col-12 px-0 mx-0">
                                Posto: <?= $evento_in_programma['Nome'] ?>
                              </div>
                            </div>

                          </div>

                          <div class="col-2 px-0 mx-0">

                            <div class="row px-0 mx-0">
                              <div class="col-12 px-0 mx-0">
                                N. biglietti <?= $evento_in_programma['Acquistati'] ?>
                              </div>
                            </div>

                          </div>
                        </div>
                      <?php
                      endforeach;
                    ?>


                  </div>

                  <?php
                }
              ?>

            </div>
          </div>
        </div>
      </div>
    </div>
</section>