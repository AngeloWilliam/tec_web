﻿<section>
	<div class="container-fluid">
		<div class="row" >
			<div class="col mainDiv" id="supportDiv" >
				<h1>Supporto / FAQ</h1>
				<p>
					<h3>Ho un problema con l'accesso al mio account!</h3><br>
					Nella sezione Login vi è un collegamento "Dimenticato la password?" che la porterà ad una pagina per il recupero della password. Se questo non dovesse bastare la invitiamo a contattarci all'indirizzo supporto.eventi@gmail.com per un aiuto diretto.
				</p>
				<p>
					<h3>Ho comprato dei biglietti tempo fa ma non ricordo come si chiama l'evento, come faccio?</h3><br>
					Nella pagina del suo profilo vi è la lista con tutti i biglietti che ha acquistato e dei loro eventi.
				</p>
				<p>
					<h3>Apprezzo molto il sito, c'è un social su cui posso seguire degli aggiornamenti?</h3><br>
					In fondo alla pagina trova la lista dei nostri contatti da seguire e a cui può rivolgersi in caso di problemi.	
				</p>
				<p>
					<h3>Ho un problema ma non riesco a trovare una soluzione nelle FAQ, come faccio?</h3><br>
					La preghiamo di contattarci all'indirizzo email supporto.eventi@gmail.com in caso riscontri problemi non specificati nelle FAQ.
				</p>
			</div>
		</div>
	</div>
</section>