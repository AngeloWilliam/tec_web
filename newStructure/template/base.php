<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <?php
      if(isset($templateParams["stylesheet"])):
        foreach($templateParams["stylesheet"] as $stylesheet):
          ?>
          <link <?php echo $stylesheet; ?>>
        <?php
        endforeach;
      endif;
      if(isset($templateParams["js"])):
        foreach($templateParams["js"] as $script):
          ?>
          <script <?php echo $script; ?>></script>
        <?php
        endforeach;
      endif;
    ?>
<!--    <link rel="stylesheet" href="./../css/cssUnico.css">-->
    <title><?php echo $templateParams["title"]; ?></title>
  </head>

  <body>
    <?php
      if(isset($templateParams["navbar"])) {
        require($templateParams["navbar"]);
      }
      if(isset($templateParams["pageRequested"])) {
        require($templateParams["pageRequested"]);
      }
      if(isset($templateParams["footer"])) {
        require($templateParams["footer"]);
      }
    ?>
  </body>
</html>
