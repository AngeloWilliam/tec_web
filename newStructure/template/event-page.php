
          <div class="row">
            <div class="col-12 collonna_titolo">
              <h1 class="titolo_evento">
                <?php
                  echo $templateParams["NomeEvento"];
                ?>
              </h1>
              <?php
                if($_SESSION["Id"] == $templateParams["IdOrganizzatore"]) {
                  echo '<a href="./event_creation.php?alter_event=' . $templateParams["Id"] . '"><button type="button" class="btn btn-primary" >Modifica</button></a>';
                }
              ?>
              <hr class="hrstyle">
            </div>
          </div>


          <div class="row justify-content-around">
            <div class="col-sm-6">
              <div class="pre-scrollable">
                <div class="row flex-row flex-nowrap">
                  <?php
                    foreach($templateParams["Immagine"] as $img) {
                      echo '<div class="col-12">
										<img src="' . IMG_DIR . $img . '" class="event_image">
									</div>';
                    }
                  ?>
                </div>
                <div>
                  <!-- <img src="<?= IMG_DIR . $templateParams["Immagine"][0] ?>" alt="" class="event_image">-->
                  <!--              <div class="pre-scrollable">
                  <!--                <div class="container-fluid" style="width: 600px;">
                  <!--                  <div class="row flex-row flex-nowrap">
                  <!--                    <div class="col-12">
                  <!--                      <img src="./../event/Immagini/prova1.jpg">
                  <!--                    </div>-->

                </div>
              </div>


              <!-- [EED] RIGA DATI EVENTO E DESCRIZIONE -->
              <div class="row eed">
                <div class="col-sm-auto">
                  <h2>Data inizio</h2>
                  <p>
                    <?php
                      echo $templateParams["DataInizio"];
                    ?>
                  </p>

                  <?php
                    if(isset($templateParams["DataFine"])):
                      ?>
                      <h2>Data Fine</h2>
                      <p>
                        <?php
                          echo $templateParams["DataFine"];
                        ?>
                      </p>
                    <?
                    endif;
                  ?>

                  <h2>Luogo</h2>
                  <p>
                    <?php
                      echo $templateParams["Luogo"];
                    ?>
                  </p>

                  <div class="col-sm-auto">
                    <h2 id="desc">Descrizione</h2>
                    <p>
                      <?php
                        echo $templateParams["Descrizione"];
                      ?>
                    </p>
                  </div>
                </div>
                <!-- FINE [EED] -->

                <div class="row">
                  <div class="col-sm-4 text-center">
                    <select class="form-control" id="ticketsCost">
                      <?php
                        foreach($templateParams["Tickets"] as $ticket) {
                          $disabled = "";
                          if($ticket["NumVenduti"] >= $ticket["TotBiglietti"]) {
                            $disabled = " disabled";
                          }
                          echo '<option value="' . $ticket["Prezzo"] . '|' . $ticket["Id"] . '"' .
                            $disabled . '>' .
                            $ticket["Nome"] .
                            ' - ' . $ticket["Prezzo"]
                            . ' €</option>';
                        }
                      ?>
                    </select>
                  </div>
                  <div class="col-sm-2 text-center">
                    <select class="form-control" id="ticketsQuantity">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                    </select>

                    <div class="col-sm-6 text-center">

                      <button type="button" class="btn btn-primary" onclick="wantPutTickets()">
                        Aggiungi al
                        carrello!
                      </button>

                    </div>

                    <div class="12">
                      <?= InfoPrinting ::print_preformatted_array_info("Template Params",
                                                                       $_SESSION) ?>
                    </div>
                  </div>
