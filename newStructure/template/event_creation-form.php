﻿<div class="row justify-content-center align-items-center" id="main_content_row">
	<div class="col-sm-7" id="creationdiv">
		<div class="h-100 mb-0 py-2 jumbotron">
			<div id="myCarousel" class="my-3 px-1 py-2 h-100 carousel slide" data-interval="0" data-wrap="false">
				<form action="#" class="h-100" method="post" id="form_evento" enctype="multipart/form-data">
					<!-- Wrapper for slides -->
					<div class="px-1 h-100 carousel-inner" role="listbox">
						<!-- INSERIMENTO NOME EVENTO -->
						<div class="h-100 carousel-item active">
							<div class="h-50 row">
								<div class="mb-3 col">
									<h1>Dai un nome all'evento:</h1>
								</div>
							</div>
								<div class="h-50 row">
									<div class="col">
										<?php
											if($_SESSION["alter_event"]){
												$input= '<input type="text" name="nome evento"
												id="nome evento" value="';
												$input = $input . $eventInfo[0]["NomeEvento"] . '" required>';
												echo $input;
											}
											else{
											echo '<input type="text" placeholder="Nome evento" name="nome evento"
												id="nome evento" required>';
											}
										?>
								</div>
							</div>
						</div>
						<!-- INSERIMENTO GIORNO DI INIZIO E DI FINE EVENTO-->
						<div class="carousel-item">
							<div class="row">
								<div class="mb-2 col">
									<h1>Definisci un periodo:</h1>
									<?php
										if($_SESSION["alter_event"] && $eventInfo[0]["DataInizio"] != $eventInfo[0]["DataFine"]){
											echo '	<label><input type="radio" name="period" id="radioOne" value="one" onclick="hideDate()" />Un giorno</label>
													<label><input type="radio" name="period" id="radioMultiple" value="multiple" onclick="hideDate()" checked/>Più giorni</label>';	
										}
										else{
											echo '	<label><input type="radio" name="period" id="radioOne" value="one" onclick="hideDate()" checked/>Un giorno</label>
													<label><input type="radio" name="period" id="radioMultiple" value="multiple" onclick="hideDate()" />Più giorni</label>';
										}
									?>
								</div>
								<div class="w-100"></div>
								<div class="col">
									<p>Inizio:</p>
									<?php
										if($_SESSION["alter_event"]){
											$input = '<input type="date" id="inizio evento" name="inizio evento" value="' . $eventInfo[0]["DataInizio"] . '" required>';
											echo $input;
										}
										else{
											echo '<input type="date" id="inizio evento" name="inizio evento" required>';
										}
									?>
								</div>
								<div class="col" id="divHide" style="display: none;">
									<p>Fine:</p>
									<?php
										if($_SESSION["alter_event"]){
											$input = '<input type="date" id="fine evento" name="fine evento" value="' . $eventInfo[0]["DataFine"] . '" required>';
											echo $input;
										}
										else{
											echo '<input type="date" id="fine evento" name="fine evento" required>';
										}
									?>
								</div>
							</div>
						</div>
						<!-- INSERIMENTO LUOGO EVENTO -->
						<div class="h-100 carousel-item">
							<div class="h-50 row">
								<div class="mb-3 col">
									<h1>In che luogo si terrà?</h1>
								</div>
							</div>
							<div class="h-50 row">
								<div class="col">
									<input type="text" placeholder="Luogo" name="luogo evento" id="luogo evento"
										required>
								</div>
							</div>
						</div>
						<!-- INSERIMENTO NOME ORGANIZZATORE EVENTO -->
						<div class="h-100 carousel-item">
							<div class="h-50 row">
								<div class="mb-3 col">
									<h1>Organizzatore</h1>
								</div>
							</div>
							<div class="h-50 row">
								<div class="col-2">
									<div class="organizzatore">RedBull</div>
								</div>
								<div class="col">
									<button class="cambio">Vorrei apparisse un nome differente</button>
								</div>
							</div>
						</div>
						<!-- INSERIMENTO DESCRIZIONE EVENTO -->
						<div class="h-100 carousel-item">
							<div class="h-50 row">
								<div class="col">
									<h1>Descrizione</h1>
									<p>Ricordati di inserire l'orario</p>
								</div>
							</div>
							<div class="h-50 row">
								<div class="col">
									<textarea rows="4" cols="30" name="comment" form="form_evento"
										placeholder="Descrizione" required></textarea>
								</div>
							</div>
						</div>
						<!-- INSERIMENTO IMMAGINE EVENTO -->
						<div class="h-100 carousel-item">
							<div class="h-50 row">
								<div class="col">
									<h1>Per ultima cosa, carica una foto!</h1>
									<p>Possibilmente in proporzione 16:9</p>
								</div>
							</div>
							<div class="h-50 row">
								<div class="col">
									<div class="custom-file mb-3">
										<!--<input type="file" id="userfile" name="userfile" required><br />-->
										<input multiple type="file" class="custom-file-input" id="fileName"
											name="fileName[]" required>
										<label class="custom-file-label" for="customFile">Choose file</label>
									</div>
								</div>
							</div>
						</div>
						<!-- FINE INSERIMENTO IMMAGINE -->
					</div>
					<!-- FINE CAROUSEL INNER -->
				</form>
					<!-- FINE MYCAROUSEL -->
			</div>
		<div class="my-3 h-100 row justify-content-around">
			<div class="col-1">
				<button href="#myCarousel" role="button" data-slide="prev">prev</button>
			</div>
			<div class="col-1">
				<button type="submit" form="form_evento" name="submit">CREA!</button>
			</div>
			<div class="col-1">
				<button href="#myCarousel" role="button" data-slide="next">next</button>
			</div>
		</div>
		</div>
	</div>
</div>

