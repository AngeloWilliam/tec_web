<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <?php
      if(isset($templateParams["stylesheet"])):
        foreach($templateParams["stylesheet"] as $stylesheet):
          ?>
          <link <?php echo $stylesheet; ?>>
        <?php
        endforeach;
      endif;
      if(isset($templateParams["js"])):
        foreach($templateParams["js"] as $script):
          ?>
          <script <?php echo $script; ?>></script>
        <?php
        endforeach;
      endif;
    ?>
<!--    <link rel="stylesheet" href="./../css/cssUnico.css">-->
    <title><?php echo $templateParams["title"]; ?></title>
  </head>

  <body>
    <?php
      if(isset($templateParams["navbar"])) {
        require($templateParams["navbar"]);
      }
	?>
	<?php echo '<section id=' . $templateParams["pageId"] .'"';?>
		<div class="container-fluid">
			<div class="row no-gutters" id="main_content_row">
				<div class="col-lg-8 col-xl-8">
					<div id="jumbotron" class="jumbotron mycustom-jumbotron">
						<?php
						  if(isset($templateParams["pageRequested"])) {
							require($templateParams["pageRequested"]);
						  }
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php
      if(isset($templateParams["footer"])) {
        require($templateParams["footer"]);
      }
    ?>
  </body>
</html>
