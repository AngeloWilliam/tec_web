﻿<?php

	require_once "bootstrap.php";
	require_once "init_stylesheet_script.php";
	require_once "utils/init_navbar_footer.php";

    $templateParams["stylesheet"] = get_stylesheets_as_array();
    array_push($templateParams["stylesheet"], 'rel = "stylesheet" href = "./css/event.css"');
    $templateParams["js"] = get_script_as_array();
	array_push($templateParams["js"], 'src = "./javascript/event_creation.js"');

	
    if(isset($_GET["alter_event"])){
		$_SESSION["alter_event"] = $_GET["alter_event"]["Id"];
		$eventInfo = $dbh->getSingoloEvento($_GET["alter_event"]["Id"]);
		
        $templateParams["title"] = "Events Finder - Modifica Evento";
    }
    else{
        $templateParams["title"] = "Events Finder - Creazione Evento";
	}

    $templateParams["pageRequested"] = "event_creation-formAlberto.php";
	$templateParams["pageId"] = "creationdiv";

	require 'template/base.php';
?>