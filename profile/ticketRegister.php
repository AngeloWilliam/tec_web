﻿<?php
	//Suppongo di aver id evento da qualche parte e l'id organizzatore in $_SESSION
	$_SESSION["IdOrganizzatore"]=4;
	$_SESSION["IdEvento"]=1;
?>

<div class="h-100 row justify-content-center align-items-center">
	<div class="col-8">
		<div class="jumbotron">
			<div class="row">
				<div class="col">
					<h1>Manca un'ultima cosa!</h1>
					<h3>Devi registrare almeno un tipo di biglietto:</h3>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<form>
						<div class="row align-items-center">
							<div class="col-6">
								<label>Nome del biglietto:</label>
								<input type="text" class="w-75" name="nome" id="nome">
							</div>
							<div class="col">
								<label>Numero biglietti:</label>
								<input type="number" name="totbiglietti" class="w-50" id="totbiglietti" min="1">
							</div>
							<div class="col-2 align-items-center">
								<label class="w-25">Prezzo:</label>
								<input type="number" class="w-25" name="prezzoeuro" id="prezzoeuro" min="0" max="999">
								,
								<input type="number" class="w-25" name="prezzocent" id="prezzocent" min="0" max="99">
							</div>
							<div class="col-1">
								<button class="btn btn-primary" type="button" id="btnRegister">Registra</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="pre-scrollable">
											<table class="table">
						<thead>
							<tr>
								<th scoper="col" class="col-8">Tipo Biglietto</th>
								<th scoper="col" class="text-center">Posti rimanenti</th>
								<th scoper="col" class="text-center">Posti totali</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td >Biglietto A</td>
								<td class="text-center">10</td>
								<td class="text-center">/20</td>
							</tr>
							<tr>
								<td>Biglietto B</td>
								<td class="text-center">100</td>
								<td class="text-center">/500</td>
							</tr>
						</tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>