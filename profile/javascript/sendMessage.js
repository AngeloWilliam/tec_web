﻿function fillList(data) {

    /*  Il javascript mi serve per fare modifiche alla pagina senza dover
     *  ricaricarla, ovvero senza fare richieste http. E' utile anche per associare 
     *  agli elementi delle informazioni che potrebbero servirmi.
     *  
     *  In questo specifico caso, utilizzo il javascript per associare
     *  ad ogni evento nella lista degli eventi il suo id, così che quando invio il messaggio
     *  prendo l'id da quell'elemento.
     *  
     *  Come invio il messaggio? Vai dentro simpleServRequest.php
     */
    const lista = $('#listEvent');
    data.forEach(function (item) {
        lista.append(`<option class="opzEvento" >` + item["NomeEvento"] + `</option>`);
        $("#listEvent option:last-child").data("idevento", item["Id"]);
    });
    $(".opzEvento").each(function () {
        console.log($(this).data("idevento"));
    });
}

$(document).ready(function () {
    if ($('#listEvent')) {
        $.getJSON("./../../profile/simpleServRequest.php", { "richiesta": "eventi" }, function (data) {
            console.log(data);
            fillList(data);
        });
    }


    $("#btnSend").click(function () {
        let evento = $('#listEvent option:selected').data("idevento");
        let tmp = ["messaggio" , $('#oggetto').val(), $('#messaggio').val(), evento];
        $.post({
            url: './../../profile/simpleServRequest.php', "richiesta": tmp, success: function (result) {
                console.log(result);
            }
        })
    })
});