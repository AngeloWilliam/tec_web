﻿<?php
	$templateParams["title"] = "Invio Messaggio";
	///require_once('./../bootstrap.php');
	$tipoutente="Organizzatori";
	$_SESSION["Id"]=4;
	$_SESSION["IdEvento"]=1;
	$_SESSION["tipoUtente"]=$tipoutente;

	/*	Molte delle pagine avranno la maggior parte degli elementi statici,
		ovvero che dopo averli generati all'inizio non li cambio/tolgo. Per questo
		in php controllo che tipo di utente sia quello che accede alla pagina e la creo di 
		conseguenza. 

		In questo specifico caso la pagina serve ad inviare un messaggio ad una categoria:
		se è l'admin a dover inviare il messaggio non devo fare altro che generare la pagina, 
		in caso sia un organizzatore devo aggiungere "dinamicamente" anche una lista di suoi
		eventi così che possa scrivere un messsaggio ai suoi clienti.

		Perchè non ho generato staticamente anche la lista di eventi? Vai dentro sendMessage.js
	*/
?>

<div class="h-100 row justify-content-center align-items-center">
	<div class="col-8">
		<div class="jumbotron">
			<div class="row">
				<div class="col">
					<h2>Invio Messaggio</h2>
				</div>
				<div class="col-12">
					<?php
						if($tipoutente=="Organizzatori")
							echo "<h5>Scegli ai partecipanti di quale evento inviare il messaggio:</h5>";
						else
							echo "<h5>Invia un messaggio agli organizzatori:</h5>";
					?>
				</div>
			</div>
			<?php if($tipoutente=="Organizzatori"): ?>
				<div class="row">
					<div class="col">
						<div class="form-group">
							<select class="form-control" id="listEvent">
								<?php/*
									$eventi = $dbh->getEventiOrganizzatore($_SESSION["Id"]);
									foreach($eventi as $evento){
										echo "<option>". $evento["NomeEvento"] ."</option>";
									}*/
								?>
							</select>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-12" id="input-group">
					<textarea class="form-control w-100" rows="1" id="oggetto" maxlength="20" name="oggetto" placeholder="Oggetto"></textarea>
					<textarea class="form-control w-100" rows="5" id="messaggio" maxlength="5000" name="messaggio" placeholder="Scrivi qui il tuo messaggio."></textarea>
				</div>
				<div class="col">
					<button class="btn btn-primary" type="button" id="btnSend">Invia</button>
				</div>
			</div>
		</div>
	</div>
</div>
