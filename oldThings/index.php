<?php
	/*	Punto di partenza, richiamo bootstrap che fa partire la sessione (per avere le variabili di 
	*	sessione a cui accedere da qualsiasi pagina), inizializza il collegamento al database e ha il collegamento
	*	alla pagina con le funzioni. Imposto un paio di variabili e richiedo la pagina mainStructure (non � un reindirzzamento)
	*/
	require_once "bootstrap.php";
	$templateParams["title"] = "Events Finder - Home";
	require "componentspage/mainStructure/mainStructure.php";
?>