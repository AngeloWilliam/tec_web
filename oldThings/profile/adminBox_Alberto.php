﻿<?php
	$templateParams["title"] = "Casella Admin";
	$firstmessage=0;
	$lastmessage=0;
?>

<div class="h-100 row justify-content-center align-items-center">
	<div class="col-8">
		<div class="jumbotron">
			<div class="row justify-content-center">
				<div class="col-12">
					<h2>Notifiche</h2>
					<div class="row">
						<div class="col">
							<button class="btn btn-primary" type="button" id="btnBackward"><</button>
						</div>
						<div class="col">
							<button class="btn btn-primary" type="button" id="btnForward">></button>
						</div>
				</div>
				<div class="col-12" id="messageContainer">
					<ul class="list-group">
						<li class="list-group-item">
							<button class="btn btn-primary" type="button">CiaoCiao</button>
						</li>
						<li class="list-group-item">
							<button class="btn btn-primary" type="button">CiaoCiao</button>
						</li>
						<li class="list-group-item">
							<button class="btn btn-primary" type="button">CiaoCiao</button>
						</li>
						<li class="list-group-item">
							<button class="btn btn-primary" type="button">CiaoCiao</button>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>