﻿<?php
	require_once('./../bootstrap.php');

	/*	Quando javascript deve comunicare con un server utilizza il protocollo POST o GET
		per richiamare una pagina che di fatto non verrà mai vista: questa pagina viene 
		elaborata dal server, può quindi comunicare col database e fare quello che deve fare.

		Alla fine, codifica tutto l'output necessario a javascript in formato JSON, un formato
		leggibile da javascript il quale leggerà la pagina (senza cambiare indirizzo, ricaricare la
		pagina in vista nel browser ecc...) e potrà eseguire il suo codice con le informazioni ottenute.
	*/

	if(isset($_REQUEST["richiesta"])){
		switch($_REQUEST["richiesta"][0]){
			case "eventi":
				$result = $dbh->getEventiOrganizzatore($_SESSION["Id"]);
				break;
			case "ricercaEventi":
				$tmp_result = $dbh->searchEvento($_REQUEST["richiesta"][1], $_REQUEST["richiesta"][2], $_REQUEST["richiesta"][3]);
				$result = array();
				foreach($tmp_result as $event){
					$image_event = $dbh->getEventoImages($event["Id"])[0]["Immagine"];
					if(isset($image_event)) {
						$event["Immagine"] = IMG_DIR.$image_event;
					}
					array_push($result, $event);
				}
				break;
			case "tipobiglietto":
				$result = $dbh->registerTipoBiglietto($_SESSION["IdEvento"],$_SESSION["IdOrganizzatore"],$_REQUEST["richiesta"][1], $_REQUEST["richiesta"][2],$_REQUEST["richiesta"][3]);
				break;
			case "messaggio":
				if($_SESSION["tipoUtente"]=="Organizzatori")
					$tipodestinatario="Clienti";
				else
					$tipodestinatario="Organizzatori";

				$result = $dbh->invioMessaggio($_SESSION["Id"], $_SESSION["tipoUtente"], $tipodestinatario,	$_REQUEST["richiesta"][1], $_REQUEST["richiesta"][2] , $_REQUEST["richiesta"][3]);
				break;
		}
	}
	else{
		$result=0;
	}
	
	header('Content-Type: application/json');
	echo json_encode($result);
?>