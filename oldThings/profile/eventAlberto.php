﻿<?php
	//TODO:Implementa un algoritmo per calcolare una media della grandezza delle immagini
	//verso cui ridimensionarle (con un minimo/massimo di dimensioni).
	//Per ora utilizzo un valore fisso;
	$size=600;
	$switchImg=true;
?>

<div class="h-100 row justify-content-center align-items-center">
	<div class="col-8">
		<div class="jumbotron">
			<div class="row">
				<div class="col">
					<h1>Nome Evento</h1>
					<h3>00/00/2020 - 00/00/2020</h3>
				</div>
			</div>
            <div class="row my-3">
				<div class="col container-fluid">
					<div class="row flex-nowrap overflow-auto">
						<?php for($i=0;$i<6;$i++): ?>
							<div class="col my-2 imgElem" >
								<img class="w-100" src="./../../event/Immagini/prova<?php if($switchImg==true){ echo "1"; $switchImg=false;}else{echo "2"; $switchImg=true;}?>.jpg">
							</div>
						<?php endfor; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<h3>Luogo Evento</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
						Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
						Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
						Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<table class="table">
						<thead>
							<tr>
								<th scoper="col" class="col-8">Tipo Biglietto</th>
								<th scoper="col" class="text-center">Posti rimanenti</th>
								<th scoper="col" class="text-center">Posti totali</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td >Biglietto A</td>
								<td class="text-center">10</td>
								<td class="text-center">/20</td>
							</tr>
							<tr>
								<td>Biglietto B</td>
								<td class="text-center">100</td>
								<td class="text-center">/500</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
        </div>
    </div>
</div>