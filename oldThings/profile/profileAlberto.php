﻿<?php
	require_once('./../../bootstrap.php');
	$_SESSION["tipoUtente"] = "Organizzatori";
	if($_SESSION["tipoUtente"] == "Clienti"){
		$idutente=1;
		$eventi = $dbh->getEventiByUserId($idutente);
	}
	else{
		$idutente=4;
		$eventi = $dbh->getEventiOrganizzatore($idutente);
	}
	$_SESSION["NomeUtente"]="Gigi";
?>

<div class="h-100 row justify-content-center align-items-center">
	<div class="col-8">
		<div class="jumbotron">
			<div class="row">
				<div class="col-12">
					<h1>Profilo</h1>
				</div>
				<div class="col-12">
					<p>
						<b>Nome utente:</b> <?php echo $_SESSION["NomeUtente"];?></br>
						<b>Email:</b> <?php echo $_SESSION["Mail"];?></br>
						<a href="#">Modifica password</a></br>
						<?php if($_SESSION["tipoUtente"] == "Organizzatori"): ?>
						<?php endif; ?>
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="pre-scrollable">
						<table class="table">
							<thead>
								<tr>
									<th scoper="col" class="col-7">Evento</th>
									<th scoper="col" class="col-2 text-center">Luogo</th>
									<th scoper="col" class="text-center">Inizio</th>
									<th scoper="col" class="text-center">Fine</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($eventi as $evento ): ?>
									<tr>
										<td ><?php echo $evento["NomeEvento"]; ?></td>
										<td class="text-center"><?php echo $evento["Luogo"]; ?></td>
										<td class="text-center"><?php echo $evento["DataInizio"]; ?></td>
										<td class="text-center"><?php echo $evento["DataFine"]; ?></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>