<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="./../componentspage/footer/css/footer.css">
    <link rel="stylesheet" href="./css/event_creation.css">

    <script src="./../homepage/javascript/homepage.js"></script>
    <script src="./javascript/event_creation.js"></script>
    <title>Events Finder</title>
  </head>


  <body>
    <section id="nav">
      <?php include("./../componentspage/navbar/navbarNotLogged.html") ?>
    </section>

    <section id="creazione_evento">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <h1>Crea il tuo evento</h1>
          </div>
        </div>

        <!--
        <div class="row">
          <div class="col-12">
            <div class="progress">
              <div class="progress-bar" role="progressbar" style="width:40%">
                Free Space
              </div>
              <div class="progress-bar" role="progressbar" style="width:10%">
                Warning
              </div>
              <div class="progress-bar" role="progressbar" style="width:20%">
                Danger
              </div>
            </div>
          </div>
        </div>
        -->

        <div class="row">
          <div class="col-12">

            <form action="./creation.php" method="post" id="form_evento" enctype="multipart/form-data">
              <div id="myCarousel" class="carousel slide" data-interval="0" data-wrap="false">

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">


                  <!-- INSERIMENTO NOME EVENTO -->
                  <div class="carousel-item active">
                    <div class="row">

                      <div class="col-12">
                        <h1>Come lo chiamerai?</h1>
                      </div>

                      <div class="col-12">

                        <label for="nome evento">Nome evento</label>
                        <input type="text" placeholder="Nome evento" name="nome evento"
                               id="nome evento" required>

                      </div>

                    </div>
                  </div>


                  <!-- INSERIMENTO GIORNO DI INIZIO E DI FINE EVENTO-->
                  <div class="carousel-item">

                    <div class="row">
                      <div class="col-12">
                        <h1>Che giorno avrà inizio?</h1>
                      </div>

                      <div class="col-12">
                        <label for="inizio evento"></label>
                        <input type="date" id="inizio evento" name="inizio evento" required>
                      </div>

                      <div class="col-12">
                        <h1>Che giorno finirà?</h1>
                      </div>

                      <div class="col-12">
                        <label for="fine evento"></label>
                        <input type="date" id="fine evento" name="fine evento" required>
                      </div>
                    </div>

                  </div>


                  <!-- INSERIMENTO LUOGO EVENTO -->
                  <div class="carousel-item">
                    <div class="row">
                      <div class="col-12">
                        <h1>In che luogo si terrà?</h1>
                      </div>
                      <div class="col-12">
                        <label for="luogo evento"></label>
                        <input type="text" placeholder="Luogo" name="luogo evento" id="luogo evento"
                               required>
                      </div>
                    </div>
                  </div>

                  <!-- INSERIMENTO NOME ORGANIZZATORE EVENTO -->
                  <div class="carousel-item">
                    <div class="row">
                      <div class="col-12">
                        <h1>Organizzatore</h1>
                      </div>
                      <div class="col-12">
                        <div class="organizzatore">RedBull</div>
                      </div>
                      <div class="col-12">
                        <button class="cambio">Vorrei apparisse un nome differente</button>
                      </div>
                    </div>
                  </div>

                  <!-- INSERIMENTO DESCRIZIONE EVENTO -->
                  <div class="carousel-item">
                    <div class="row">
                      <div class="col-12">
                        <h1>Descrizione</h1>
                        <p>Ricordati di inserire l'orario</p>
                      </div>
                      <div class="col-12">
                      <textarea rows="4" cols="30" name="comment" form="form_evento"
                                placeholder="Descrizione" required></textarea>
                      </div>
                    </div>
                  </div>

                  <!-- INSERIMENTO IMMAGINE EVENTO -->
                  <div class="carousel-item">
                    <div class="row">

                      <div class="col-12">
                        <h1>Per ultima cosa, carica una foto!</h1>
                        <p>Possibilmente in proporzione 16:9</p>
                      </div>

                      <div class="col-12">
                        <div class="custom-file mb-3">
                          <!--<input type="file" id="userfile" name="userfile" required><br />-->

                          <input type="file" class="custom-file-input" id="fileName"
                                 name="fileName" required>
                          <label class="custom-file-label" for="customFile">Choose file</label>

                        </div>
                      </div>

                      <div class="col-12">

                      </div>
                    </div>
                  </div>
                  <!-- FINE INSERIMENTO IMMAGINE -->

                </div>
                <!-- FINE CAROUSEL INNER -->

              </div>
              <!-- FINE MYCAROUSEL -->

            </form>

          </div>
        </div>

        </form>
        <div class="row">
          <div class="col-4">
            <button href="#myCarousel" role="button" data-slide="prev">prev</button>
          </div>

          <div class="col-4">

            <button type="submit" form="form_evento" name="submit">
             CREA!
            </button>

          </div>

          <div class="col-4">
            <button href="#myCarousel" role="button" data-slide="next">next</button>
          </div>
        </div>
      </div>
    </section>


    <section class="footer">
      <?php include("./../componentspage/footer/footer.html") ?>
    </section>
  </body>

</html>