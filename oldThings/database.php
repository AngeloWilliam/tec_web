<?php
class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }        
    }

	//Ogniqualvolta si definisce un attributo OPZIONALE significa che bisogna mettere 0(intero, non stringa)
	
	//TODO per Alberto: Non utilizzare echo ma ritornare un messaggio nei metodi pubblici sia in caso di conferma che di errore.

	/*	Ritorna l'ultimo id di una tabella passata in input. Necessari tutti i campi.
	*/
	public function getLastId($tipo){
		$query = "SELECT Id FROM $tipo ORDER BY Id DESC LIMIT 1 ;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->execute();
			$result = $stmt->get_result();
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}

	/*	Registra la partecipazione di un cliente ad un evento. Necessari tutti i campi.
	*/
	private function clientePartecipa($idcliente, $idevento){
		$query = "INSERT INTO Partecipa VALUES (?,?);";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('ii', $idcliente, $idevento);
			$stmt->execute();

			return;
		}
	}

	/*	Registra un messaggio. Necessari tutti i campi.
	*/
	private function registerMessaggio($tipodestinatario,$oggetto,$testo){
		$prova = $this->getLastId("Messaggi");
		$n= $prova[0]["Id"] + 1;
		$query = "INSERT INTO Messaggi VALUES (?,now(),?,?,?);";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione registerMessaggio.");
		else{
			$stmt->bind_param('isss', $n,$oggetto,$tipodestinatario,$testo);
			$stmt->execute();
			if($stmt->affected_rows < 0)
				return -1;
			
			return $n;
		}
	}

	/*	Registra informazioni sull'invio di un messaggio. OPZIONALE: $idevento
	*/
	private function registerInvio($idmessaggio,$idmittente, $idevento){
		$query = "INSERT INTO Invio VALUES (?,?,?);";
		$stmt = $this->db->prepare($query);
		if(!$stmt){
			die("Errore di connessione registerInvio.");
		}
		else{
			$stmt->bind_param('iii', $idmessaggio,$idmittente,$idevento);
			$stmt->execute();
			if($stmt->affected_rows < 0)
				return -1;
			
			return 1;
		}
	}

	/*	Registra informazioni sulla ricezione di un messaggio. OPZIONALE $idevento
		Se un messaggio non è stato letto il campo NotificationStatus è a -1,
		altrimenti a 1.
	*/
	private function registerRicezione($idmessaggio, $idevento){
		$query = "INSERT INTO Ricezione VALUES (?,?,?,?);";
		$notificazione = -1;
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione registerRicezione.");
		else{
			$stmt->bind_param('iiii', $idmessaggio, $idutente, $idevento,  $notificazione);
			$idlist = $this->listRicezione($idevento);
			if(!$idlist)
				return -1;
			else{
				foreach($idlist as $value){
					if($idevento){
						$idutente=$value["IdCliente"];
					}
					else{
						$idutente=$value["IdUtente"];
					}
					$stmt->execute();
				}
			}
			return 1;
		}
	}

	/* Ritorna gli utenti che devono ricevere un messaggio. OPZIONALE: $idevento
	*/
	private function listRicezione($idevento){
		if($idevento !== 0){
			$query = "SELECT IdCliente FROM Partecipa WHERE IdEvento=?;";
		}
		else
			$query = "SELECT IdUtente FROM Organizzatori;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione listRicezione.");
		else{
			if($idevento !== 0)
				$stmt->bind_param('i', $idevento);

			$stmt->execute();
			$result = $stmt->get_result();
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}

	/*	Aggiorna il numero di biglietti venduti per un tipo. Necessari tutti i campi.
	*/
	private function updateTipoBiglietto($idtipobiglietto ,$n_acquisti){
		$query = "UPDATE TipoBiglietto SET NumVenduti=NumVenduti+? WHERE Id = ?;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('ii', $n_acquisti, $idtipobiglietto);
			$stmt->execute();
			if($stmt->affected_rows < 0)
				return -1;
			else
				return 1;
		}
	}

	/*	Controlla sia acquistabile un tipo di biglietto. Necessari tutti i campi.
	*/
	private function checkTipoBiglietto($idtipobiglietto, $n_acquisti){
		$query = "SELECT NumVenduti,TotBiglietti FROM TipoBiglietto WHERE Id= ? AND NumVenduti+? <= TotBiglietti;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('ii', $idtipobiglietto, $n_acquisti);
			$stmt->execute();
			$tmp = $stmt->get_result();
			$result = $tmp->fetch_all(MYSQLI_ASSOC);
			if($result)
				return 1;
			else
				return -1;
		}
	}

	/*	Controlla i parametri in input e restituisce una stringa che identifica
		il tipo di ricerca che si vuole fare. Funzione interna, necessari tutti i campi.
	*/
	private function searchEvaluation($nomeevento, $luogo, $data){
		if($nomeevento !== "" && $luogo !== "" && $data !== "")
			return "NomeLuogoData";
		else if($nomeevento !== "" && $luogo === "" && $data === "")
			return "Nome";
		else if($nomeevento === "" && $luogo !== "" && $data === "")
			return "Luogo";
		else if($nomeevento === "" && $luogo === "" && $data !== "")
			return "Data";
		else if($nomeevento !== "" && $luogo !== "" && $data === "")
			return "NomeLuogo";
		else if($nomeevento !== "" && $luogo === "" && $data !== "")
			return "NomeData";
		else if($nomeevento === "" && $luogo !== "" && $data !== "")
			return "LuogoData";
		else
			return "Nothing";
	}

	/*	Imposta come letta una notifica. Necessari tutti i campi.
	*/
	public function setNotificationRead($idutente, $idmessaggio){
		$query = "UPDATE Ricezione SET NotificationStatus = 1 WHERE IdUtente = ? AND IdMessaggio = ?;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('ii', $idutente, $idmessaggio);
			$stmt->execute();
			if($stmt->affected_rows < 0)
				return -1;
			
			return 1;
		}
	}

	/*	Registra un utente speciale(Amministratori o Organizzatori). OPZIONALE: $partitaiva
	*/
	private function registerSpecialUser($tipoutente, $idutente, $nome, $cognome, $partitaiva){
		if($tipoutente == "Amministratori")
			$query = "INSERT INTO Amministratori VALUES (?,?,?);";
		else
			$query = "INSERT INTO Organizzatori VALUES (?,?,?,?);";

		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			if($tipoutente == "Amministratori")
				$stmt->bind_param('iss', $idutente, $nome, $cognome);
			else
				$stmt->bind_param('issi', $idutente, $nome, $cognome, $partitaiva);

			$stmt->execute();
			if($stmt->affected_rows < 0)
				return -1;
			
			return 1;
		}
	}

	/*	Registra una lista di immagini di un evento. Necessari tutti i campi.
		Anche in caso di singola immagine passare comunque un array di stringhe e 
		non direttamente la stringa.
	*/
	public function uploadImmaginiEvento($idevento, $imglist){
		$query = "INSERT INTO ImmaginiEvento VALUES (?,?);";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('is', $idevento, $img);
			foreach($imglist as $img){
				$stmt->execute();
				if($stmt->affected_rows < 0)
					return $stmt;
			}
		}
		return 1;
	}

	/*	Ritorna i tipi di biglietto di un evento, compreso di numero posti
		massimi e venduti. Necessari tutti i campi.
	*/
	public function getBigliettiEvento($idevento){
		$query = "SELECT * FROM TipoBiglietto WHERE IdEvento= ?;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('i', $idevento);
			$stmt->execute();
			$tmp = $stmt->get_result();
			return $tmp->fetch_all(MYSQLI_ASSOC);
		}
	}

	/*	Ritorna i dati di un utente. Necessari tutti i campi.
	*/
	public function getUtente($idutente, $tipoutente){
		switch($tipoutente){
			case "Clienti":
				$query = "SELECT Id, Mail, Username FROM Utenti AS U WHERE TipoUtente = ? AND U.Id = ?;";
				break;
			case "Organizzatori":
				$query = "SELECT Id, Mail, Username, Nome, Cognome FROM Utenti AS U, Organizzatori WHERE U.Id = Organizzatori.IdUtente AND U.TipoUtente = ? AND U.Id = ?;";
				break;
			case "Amministratori":
				$query = "SELECT Id, Mail, Username, Nome, Cognome FROM Utenti AS U, Amministratori WHERE U.Id = Amministratori.IdUtente AND U.TipoUtente = ? AND U.Id = ?;";
				break;
			case "Utenti":
				$query = "SELECT Id, Mail, Username FROM Utenti AS U WHERE U.Id = ?;";
				break;
			default:
				return -1;
				break;
		}
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			if($tipoutente != "Utenti")
				$stmt->bind_param('si', $tipoutente, $idutente);
			else
				$stmt->bind_param('i',  $idutente);

			$stmt->execute();
			$result = $stmt->get_result();
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}

	/*	Ritorna una lista di utenti. OPZIONALE: idutente.
	*/
	public function getListaUtenti($tipoutente, $idutente){
		switch($tipoutente){
			case "Clienti":
				if($idutente < 1)
					$query = 'SELECT Id, Mail, Username, TipoUtente FROM Utenti WHERE TipoUtente = ? ORDER BY Utenti.Id DESC LIMIT 11;';
				else
					$query = 'SELECT Id, Mail, Username, TipoUtente FROM Utenti WHERE TipoUtente = ? AND Utenti.Id < ? ORDER BY Utenti.Id DESC LIMIT 11;';
				break;
			case "Organizzatori":
				if($idutente < 1)
					$query = "SELECT Id, Mail, Username, TipoUtente, Nome, Cognome FROM Utenti, Organizzatori WHERE Utenti.Id = Organizzatori.IdUtente AND TipoUtente = ? ORDER BY Utenti.Id DESC LIMIT 11;";
				else
					$query = "SELECT Id, Mail, Username, TipoUtente, Nome, Cognome FROM Utenti, Organizzatori WHERE Utenti.Id = Organizzatori.IdUtente AND TipoUtente = ? AND Utenti.Id < ? ORDER BY Utenti.Id DESC LIMIT 11;";
				break;
			case "Amministratori":
				if($idutente < 1)
					$query = "SELECT Id, Mail, Username, TipoUtente, Nome, Cognome FROM Utenti, Amministratori WHERE Utenti.Id = Amministratori.IdUtente AND TipoUtente = ? ORDER BY Utenti.Id DESC LIMIT 11;";
				else
					$query = "SELECT Id, Mail, Username, TipoUtente, Nome, Cognome FROM Utenti, Amministratori WHERE Utenti.Id = Amministratori.IdUtente AND TipoUtente = ? AND Utenti.Id < ? ORDER BY Utenti.Id DESC LIMIT 11;";
				break;
			case "Utenti":
				if($idutente < 1)
					$query = "SELECT Id, Mail, Username, TipoUtente FROM Utenti ORDER BY Utenti.Id DESC LIMIT 11;";
				else
					$query = "SELECT Id, Mail, Username, TipoUtente FROM Utenti WHERE Utenti.Id < ? ORDER BY Utenti.Id DESC LIMIT 11;";
				break;
			default:
				return -1;
				break;
		}
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			if($tipoutente != "Utenti"){
				if($idutente < 1)
					$stmt->bind_param('s', $tipoutente);
				else
					$stmt->bind_param('si', $tipoutente, $idutente);
			}
			else if($idutente >= 1)
				$stmt->bind_param('i', $idutente);

			$stmt->execute();
			$result = $stmt->get_result();
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}

	/*	Modifica un evento di cui è passato l'id. Necessari tutti i campi.
	*/
	public function updateEvento($id, $nomeevento, $luogo, $datainizio, $datafine, $descrizione){
		$query = "UPDATE Eventi SET NomeEvento = ?,  Luogo = ?, DataInizio = ?, DataFine = ?, Descrizione = ? WHERE Id = ?;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('sssssi', $nomeevento, $luogo, $datainizio, $datafine, $descrizione, $id);
			$stmt->execute();
			if($stmt->affected_rows < 0)
				return $stmt->error;
			else
				return $n;
		}
	}

	/*	Registra un utente. OPZIONALE: $nome, $cognome, $partitaiva 
	*/
	public function registerUtente($tipoutente, $nomeutente, $email, $password, $nome, $cognome, $partitaiva){
		$query = "INSERT INTO Utenti VALUES (?,?,?,?,?);";
		$stmt = $this->db->prepare($query);
		$prova = $this->getLastId("Utenti");
		$n= $prova[0]["Id"] + 1;
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('issss', $n, $email, md5($password), $nomeutente, $tipoutente);
		}
		$stmt->execute();
		if($stmt->affected_rows < 0)
			return array(-1,"errno" => 1062, $stmt);
		else if($tipoutente != "Clienti"){
			$result = $this->registerSpecialUser($tipoutente, $n, $nome, $cognome, $partitaiva);
			if($result < -1)
				return array(-1, $result);
		}
		return array($n);
	}

	/*	Registra un evento. Necessari tutti i campi.
		In caso l'evento duri un singolo giorno, scrivere il giorno dell'evento
		sia per la data d'inizio che per la data di fine. 
		Ritorna l'Id dell'evento.
	*/
	public function registerEvento($idorganizzatore, $nomeevento, $luogo, $datainizio, $datafine, $descrizione){
		$query = "INSERT INTO Eventi VALUES (?,?,?,?,?,?,?);";
		$stmt = $this->db->prepare($query);
		$prova = $this->getLastId("Eventi");
		$n= $prova[0]["Id"] + 1;
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('iisssss', $n, $idorganizzatore, $nomeevento, $luogo, $datainizio, $datafine, $descrizione);
			$stmt->execute();
			if($stmt->affected_rows < 0)
				return $stmt->error;
			else
				return $n;
		}
	}

	/*	Elimina una lista di immagini da un evento. Necessari tutti i campi.
		Anche in caso di singola immagine passare comunque un array di stringhe e 
		non direttamente la stringa.
	*/
	public function deleteImmaginiEvento($idevento, $imglist){
		$query = "DELETE FROM ImmaginiEvento WHERE IdEvento = ? AND Immagine = ?;";
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('is', $idevento, $img);
			foreach($imglist as $img){
				$stmt->execute();
				if($stmt->affected_rows < 0)
					return $stmt;
			}
		}
		return;
	}

	/* Aggiorna un account. Necessari tutti i campi.
	*/
	public function updateAccount($idutente, $nomeutente, $mail, $password){
		$query = "UPDATE Utenti SET Username = ?, Mail = ?, Password = ? WHERE Id = ?;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('sssi', $nomeutente, $mail, md5($password), $idutente);
			$stmt->execute();
			if($stmt->affected_rows < 0)
				return $stmt->error;
			else
				return $n;
		}
	}

	/*	Registra l'acquisto dei biglietti. Necessari tutti i campi.
		La funzione registra l'acquisto di un numero variabile di biglietti ma 
		solo di un tipo, se si acquistano biglietti di diversi tipi è necessario richiamare
		più volte la funzione.
	*/
	public function acquistoBiglietti($idcliente,$idtipobiglietto,$idevento, $nbiglietti){
		if($this->checkTipoBiglietto($idtipobiglietto,$nbiglietti) < 0)
			return -1;
		$prova = $this->getLastId("Biglietti");
		$n= $prova[0]["Id"] + 1;
		$query = "INSERT INTO Biglietti VALUES (?,now(),?,?);";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('iii', $n, $idcliente,$idtipobiglietto);
			$acquisti = $nbiglietti;
			$nbiglietti = $nbiglietti+$n;
			for(;$n < $nbiglietti;$n++){
				$stmt->execute();
				if($stmt->affected_rows < 0)
					return -1;
			}
			$this->clientePartecipa($idcliente, $idevento);
			$this->updateTipoBiglietto($idtipobiglietto, $acquisti);
			return 1;
		}
	}

	/*	Invia un messaggio da un utente a una categoria di utenti. OPZIONALE: idevento.
		I tipo mittente sono Amministratori, Organizzatori, Clienti. I messaggi possono essere inviati:
			-da Organizzatori a Clienti di un evento(necessario l'idevento);
			-da Amministratori a Organizzatori;
	*/
	public function invioMessaggio($idmittente, $tipomittente, $tipodestinatario,$oggetto, $testo, $idevento){
		$idmessaggio = $this->registerMessaggio($tipodestinatario,$oggetto,$testo);
		if($idmessaggio > 0){
			$this->registerInvio($idmessaggio,$idmittente,$idevento);
			$this->registerRicezione($idmessaggio,$idevento);
			return 1;
		}
		else
			return -1;
	}

	/*	Registra un tipo di biglietto per un evento. Necessari tutti i campi.
		Il prezzo non può superare i 999,99 euro.
	*/
	public function registerTipoBiglietto($idevento, $idorganizzatore, $nome, $prezzo,$totbiglietti){
		$query = "INSERT INTO TipoBiglietto VALUES (?,?,?,?,?,?,?);";
		$stmt = $this->db->prepare($query);
		$prova = $this->getLastId("TipoBiglietto");
		$n= $prova[0]["Id"] + 1;
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('iiisdii', $n, $idevento, $idorganizzatore, $nome, $prezzo, $totbiglietti, $tmp=0);
			$stmt->execute();
			if($stmt->affected_rows < 0)
				return -1;

			return 1;
		}
	}

	/*	Ritorna gli n eventi che partono più in futuro. Necessari tutti i campi.
	*/
	public function getEventiShowcase($n){
		$query = "SELECT * FROM Eventi ORDER BY DataInizio DESC LIMIT ? ;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('i', $n);
			$stmt->execute();
			$result = $stmt->get_result();
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}

	public function getEventiByUserId($idutente){
		$query = "SELECT * FROM Eventi AS E,Partecipa AS P WHERE E.Id = P.IdEvento;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('i',$idevento);
			$stmt->execute();
			$result = $stmt->get_result();
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}

	/*	Ritorna tutti gli eventi ordinati per Id partendo dal maggiore. OPZIONALE: $idevento.
		Utilizzare $idevento per non partire dagli ultimi eventi inseriti.
	*/
	public function getEventiAdmin($idevento){
		$query = "SELECT * FROM Eventi WHERE Id < ? ORDER BY Id LIMIT 20;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('i',$idevento);
			$stmt->execute();
			$result = $stmt->get_result();
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}

	/*	Ritorna tutti gli eventi creati da un organizzatore. Necessari tutti i campi.
	*/
	public function getEventiOrganizzatore($idorganizzatore){
		$query = "SELECT * FROM Eventi WHERE IdOrganizzatore = ?;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('i',$idorganizzatore);
			$stmt->execute();
			$result = $stmt->get_result();
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}

	/*	Ritorna tutte le informazioni di un evento. Necessari tutti i campi.
	*/
	public function getSingoloEvento($idevento){
		$query = "SELECT * FROM Eventi WHERE Id = ? ;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('i', $idevento);
			$stmt->execute();
			$result = $stmt->get_result();
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}

  /*	Ritorna tutte le informazioni di un tipo di biglietto per uno specifico evento.
      Necessari tutti i campi.
  */
  public function getSingoloBiglietto($idbiglietto){
    $query = "SELECT * FROM TipoBiglietto WHERE Id = ? ;";
    $stmt = $this->db->prepare($query);
    if(!$stmt)
      die("Errore di connessione.");
    else{
      $stmt->bind_param('i', $idbiglietto);
      $stmt->execute();
      $result = $stmt->get_result();
      return $result->fetch_all(MYSQLI_ASSOC);
    }
  }

	/*	Ritorna tutti gli eventi che contengono la parola data nel nome dell'evento. Necessari tutti i campi.
	*/
	public function getEventobyName($nomeevento){
		$query = "SELECT * FROM Eventi WHERE NomeEvento LIKE ? ;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$nomeevento = "%" . $nomeevento . "%";
			$stmt->bind_param('s', $nomeevento);
			$stmt->execute();
			$result = $stmt->get_result();
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}

	/*	Ritorna tutti gli eventi che contengono la parola data nel luogo dell'evento. Necessari tutti i campi.
	*/
	public function getEventobyLuogo($luogo){
		$query = "SELECT * FROM Eventi WHERE Luogo LIKE ? ;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$luogo = "%" . $luogo . "%";
			$stmt->bind_param('s', $luogo);
			$stmt->execute();
			$result = $stmt->get_result();
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}

	/*	Ritorna tutti gli eventi che soddisfano i parametri di ricerca. OPZIONALE: tutti i campi, è necessario
		che almeno uno sia riempito.
		La data viene cercata nell'intervallo di tempo in cui si svolge un evento.
		TODO per Alberto:definire un intervallo di tempo entro cui cercare un evento.
	*/
	public function searchEvento($nomeevento, $luogo, $data){
		$evaluation = $this->searchEvaluation($nomeevento, $luogo, $data);
		$data2 = $data;
		switch($evaluation){
			case "NomeLuogoData":
				$query = "SELECT * FROM Eventi WHERE NomeEvento LIKE ? AND Luogo LIKE ? AND DataInizio <= ? AND DataFine >= ? ;";
				$stmt = $this->db->prepare($query);
				if(!$stmt)
					die("Errore di connessione.");
				else{
					$nomeevento = "%" . $nomeevento . "%";
					$luogo = "%" . $luogo . "%";
					$stmt->bind_param('ssss', $nomeevento, $luogo, $data, $data);
				}
				break;
			case "Nome":
				$query = "SELECT * FROM Eventi WHERE NomeEvento LIKE ? ;";
				$stmt = $this->db->prepare($query);
				if(!$stmt)
					die("Errore di connessione.");
				else{
					$nomeevento = "%" . $nomeevento . "%";
					$stmt->bind_param('s', $nomeevento);
				}
				break;
			case "Luogo":
				$query = "SELECT * FROM Eventi WHERE Luogo LIKE ? ;";
				$stmt = $this->db->prepare($query);
				if(!$stmt)
					die("Errore di connessione.");
				else{
					$luogo = "%" . $luogo . "%";
					$stmt->bind_param('s', $luogo);
				}
				break;
			case "Data":
				$query = "SELECT * FROM Eventi WHERE DataInizio <= ? AND DataFine >= ? ;";
				$stmt = $this->db->prepare($query);
				if(!$stmt)
					die("Errore di connessione.");
				else{
					$stmt->bind_param('ss', $data, $data);
				}
				break;
			case "NomeLuogo":
				$query = "SELECT * FROM Eventi WHERE NomeEvento LIKE ? AND Luogo LIKE ? ;";
				$stmt = $this->db->prepare($query);
				if(!$stmt)
					die("Errore di connessione.");
				else{
					$nomeevento = "%" . $nomeevento . "%";
					$luogo = "%" . $luogo . "%";
					$stmt->bind_param('ss', $nomeevento, $luogo);
				}
				break;
			case "NomeData":
				$query = "SELECT * FROM Eventi WHERE NomeEvento LIKE ? AND DataInizio <= ? AND DataFine >= ?;";
				$stmt = $this->db->prepare($query);
				if(!$stmt)
					die("Errore di connessione.");
				else{
					$nomeevento = "%" . $nomeevento . "%";
					$stmt->bind_param('sss', $nomeevento, $data, $data);
				}
				break;
			case "LuogoData":
				$query = "SELECT * FROM Eventi WHERE Luogo LIKE ? AND DataInizio <= ? AND DataFine >= ?;";
				$stmt = $this->db->prepare($query);
				if(!$stmt)
					die("Errore di connessione.");
				else{
					$luogo = "%" . $luogo . "%";
					$stmt->bind_param('sss', $luogo, $data, $data);
				}
				break;
			case "Nothing":
				$this->getEventi("Nuovi", 10);
				break;
			default:
				return;
				break;
		}
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	/*	Controlla che le credenziali inserite siano corrette. Necessari tutti i campi.
	*/
	public function checkCredentials($mail, $password){
		$query = "SELECT * FROM Utenti WHERE Mail = ? AND Password = ? ;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('ss', $mail, $password);
			$stmt->execute();
			$result = $stmt->get_result();
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}
	
	/*	Ritorna tutte le immagini di un evento. Necessari tutti i campi.
	*/
	public function getEventoImages($idevento){
		$query = "SELECT * FROM ImmaginiEvento WHERE IdEvento = ?;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('i', $idevento);
			$stmt->execute();
			$result = $stmt->get_result();
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}

	public function getImages(){
		$query = "SELECT * FROM ImmaginiEvento;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('i', $idevento);
			$stmt->execute();
			$result = $stmt->get_result();
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}

	/*	Ritorna n eventi che corrispondo ad una categoria. Necessari tutti i campi.
		Le categorie sono:
			-BigliettiEsaurimento: hanno pochi biglietti rimanenti;
			-Nuovi:	aggiunti recentemente;
			-InCorso: si stanno svolgendo in questo momento;
			-Popolari (ancora in sviluppo);
	*/
	public function getEventi($tipoeventi, $numeroeventi){
		switch($tipoeventi){
			case "Popolari":
				break;
			case "BigliettiEsaurimento":
				$query = "SELECT * FROM Eventi E, TipoBiglietto TB WHERE E.Id = TB.IdEvento AND NumVenduti > TotBiglietti*0.9 AND NumVenduti <> TotBiglietti LIMIT ?;";
				break;
			case "Nuovi":
				$query = "SELECT * FROM Eventi ORDER BY Id DESC LIMIT ? ;";
				break;
			case "InCorso":
				$query = "SELECT * FROM Eventi WHERE DataInizio <= now() AND DataFine >= now() ORDER BY DataInizio DESC LIMIT ? ;";
				break;
		}
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('i', $numeroeventi);
			$stmt->execute();
			$result = $stmt->get_result();
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}

	/*	Elimina un evento. Necessari tutti i campi.
	*/
	public function deleteEvento($idevento){
		$query = "DELETE FROM Eventi WHERE Id = ?;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('i', $idevento);
			$stmt->execute();
			if($stmt->affected_rows < 0)
				return -1;

			return 1;
		}
	}

	/*	Elimina un utente (che non sia un admin). Necessari tutti i campi
	*/
	public function deleteUser($idutente){
		$query = "DELETE FROM Utenti WHERE TipoUtente <> 'Amministratori';";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt -> bind_param('i', $idutente);
			$stmt -> execute();
			if($stmt -> affected_rows < 0) {
				return -1;
			}
			return 1;
		}
	}

	/*	Ritorna le notifiche non lette dell'utente. Necessari tutti i campi.
	*/
	public function checkNotifications($idutente){
		$query = "SELECT Oggetto FROM Ricezione, Messaggi WHERE Ricezione.IdMessaggio = Messaggi.Id AND Ricezione.IdUtente = ? AND NotificationStatus < 0 ;";
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			$stmt->bind_param('s', $idutente);
			$stmt->execute();
			$result = $stmt->get_result();
			//$this->setNotificationsRead($idUtente, $tipoUtente);
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}

	/*	Ritorna i messaggi di un utente. OPZIONALE: $idmessaggio.
		La funziona ritorna 11 messaggi, per ricevere quelli successivi inserire
		l'id del messaggio più vecchio.
	*/
	public function getMessages($idutente, $idmessaggio){
		if($idmessaggio < 1)
			$query = "SELECT M.Id, M.DataInvio, M.Oggetto, M.Testo, R.NotificationStatus, E.NomeEvento FROM Ricezione AS R, Messaggi AS M, Eventi AS E WHERE R.IdMessaggio = M.Id AND R.IdUtente = ? AND E.Id = R.IdEvento ORDER BY M.Id DESC LIMIT 11;";
		else
			$query = "SELECT M.Id, M.DataInvio, M.Oggetto, M.Testo, R.NotificationStatus, E.NomeEvento FROM Ricezione AS R, Messaggi AS M, Eventi AS E WHERE R.IdMessaggio = M.Id AND R.IdUtente = ? AND E.Id = R.IdEvento AND M.Id < ? ORDER BY M.Id DESC LIMIT 11;";
		
		$stmt = $this->db->prepare($query);
		if(!$stmt)
			die("Errore di connessione.");
		else{
			if($idmessaggio < 1)
				$stmt->bind_param('i', $idutente);
			else
				$stmt->bind_param('ii', $idutente, $idmessaggio);

			$stmt->execute();
			$result = $stmt->get_result();
			return $result->fetch_all(MYSQLI_ASSOC);
		}
	}

  /**
   * Ritorna le notifiche non lette dell'utente.
   * Necessari tutti i campi.
   *
   * @param int $idUtente ID dell'utente
   * @param string $tipoUtente Tipologia utente, valori accettati [Clienti, Organizzatori]
   *
   * @return array
  */
  public function checkNotificationsPROVANGELO($idUtente, $tipoUtente){
    switch($tipoUtente){
      case "Clienti":
        $query = "SELECT * FROM Ricezione_C, Messaggi WHERE Ricezione_C.IdMessaggio = Messaggi.Id AND Ricezione_C.IdCliente = ?;";
        break;
      case "Organizzatori":
        $query = "SELECT Oggetto FROM Ricezione_O, Messaggi WHERE Ricezione_O.IdMessaggio = Messaggi.Id AND Ricezione_O.IdOrganizzatore = ? AND NotificationsStatus < 0 ;";
        break;
      default:
        echo "Errore scelta notifiche utente.";
        break;
    }
    $stmt = $this->db->prepare($query);
    if(!$stmt)
      die("Errore di connessione.");
    else{
      $stmt->bind_param('s', $idUtente);
      $stmt->execute();
      $result = $stmt->get_result();
      //$this->setNotificationsRead($idUtente, $tipoUtente);
      return $result->fetch_all(MYSQLI_ASSOC);
    }
  }
}
?>