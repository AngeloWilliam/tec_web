<?php

  require_once "bootstrap.php";
  require_once "init_stylesheet_script.php";
  require_once "utils/init_navbar_footer.php";
  if(isUserLoggedIn()) {

    $templateParams["stylesheet"] = get_stylesheets_as_array();
    $templateParams["js"] = get_script_as_array();
    $templateParams["title"] = "Events Finder - Profilo";
    $templateParams["pageRequested"] = "profile-page.php";
	$templateParams["pageId"] = "profile";

    $templateParams['eventi'] = get_array_event_with_image($dbh ->
    getEventiByUserId2($_SESSION['Id']));

    array_push($templateParams["stylesheet"], 'rel = "stylesheet" href = "./css/profile.css"');

    require 'template/base.php';

  } else {
    header('location : ./');
  }
?>

