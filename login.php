<?php

  require_once "bootstrap.php";
  require_once "init_stylesheet_script.php";
  require_once "utils/init_navbar_footer.php";
  //Base Template


  $templateParams["stylesheet"] = get_stylesheets_as_array();
  array_push($templateParams["stylesheet"], 'rel = "stylesheet" href = "./css/login.css"');
  $templateParams["js"] = get_script_as_array();

  if(isset($_POST["email"]) && isset($_POST["psw"])){
    $login_result = $dbh->checkCredentials($_POST["email"], $_POST["psw"]);
    if(count($login_result)==0){
      //Login fallito
      $templateParams["errorelogin"] = "Credenziali non corrette";
    }
    else{
      registerLoggedUser($login_result[0]);
    }
  }

  if(!isUserLoggedIn()){
    $templateParams["title"] = "Events Finder - Login";
    $templateParams["pageRequested"] = "login-form.php";
	$templateParams["pageId"] = "login";
  } else {
    header('Location: http://eventsfinder.altervista.org/index.php');
  }


  require 'template/base.php';
?>